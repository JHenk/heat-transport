# Anleitung

Das Programm *heat-transport* ist eine genaue numerische Umsetzung des Spiels *The Heat is On!*. Zum Ablauf des Programm ist deshalb die Anleitung des Spiels zu Rate zu ziehen.

## Ausführung des Programm
Das ausführbare Programm befindet sich im Ordner *bin*. Es ist ratsam, das Programm im Ordner *run* auszuführen; dann befinden sich Ein- und Ausgabedateien im selben Verzeichnis.

Der Befehl

    ../bin/heat-transport

liest die Eingabedatei *heat-transport.in* und erzeugt die Ausgabedatei *heat-transport.out*. Beide sind editierbar (ASCII text).

## Die Eingabedatei *heat-transport.in*
Das Datenformat wird anhand eines Beispiel erläutert.

###  1. Zeile

    100000  250    // number of steps, output control

Die Anzahl der Spielzüge (*number of steps*) beträgt 100000, wobei jeder 250ste Zustand in die Ausgabedatei geschrieben wird (*output control*).

### 2. Zeile

    100      10    // system width & length (cells x cells)

Die beiden Angaben legen die Anzahl der Zeilen (*width*; hier: 100) und die Anzahl der Spalten (*length*; hier: 10) des Streifens fest. Um das Spiel *The Heat is On!* zu simulieren, sind beide Werte auf 10 zu setzen.

### 3. Zeile

    1.0 0.0 0.0    // filling probabilities for FILLED state (left bath, stripe, right bath; 0.0 ... 1.0)

Die drei Werte legen die Energie - und damit die Temperatur - des linken Bades, des Streifens und des rechten Bades fest. Es ist die Wahrscheinlichkeit angegeben, mit der ein Feld mit einem dunklen Chip (*FILLED*) besetzt wird; die Werte müssen zwischen 0 und 1 liegen.

Im Spiel befinden sich im linken Bad nur dunkle Chips; deshalb ist die zugehörige Besetzungswahrscheinlichkeit 1. Außerdem befinden sich im Streifen und im rechten Bad nur helle Chips; deshalb sind beide Wahrscheinlichkeiten 0.

Mit geeigneter Wahl der Wahrscheinlichkeiten lassen sich Systeme, die in der Spielanleitung vorgeschlagen werden, untersuchen.

## Die Ausgabedatei *heat-transport.out*
Die Ausgabedatei enthält die Nummer des Spielzugs (1. Spalte), die Anzahl der bisher durchgeführten Vertauschungen von Spielchips (2. Spalte) sowie in den anschließenden Spalten für jede Spalte die relative Anzahl der dunklen Chips.

      250         9    0.024263    0.009243           0           0           0           0           0           0           0           0
      500        23    0.059481    0.013733  0.00055888           0           0           0           0           0           0           0
    99500     35745     0.79127     0.70165     0.60804     0.51734     0.44308     0.36965     0.29951     0.22935     0.16317     0.10801
    99750     35852     0.79127     0.70178     0.60801     0.51756     0.44322     0.37015     0.29993     0.22965     0.16346     0.10822

## Grafische Darstellung
Die Ausgabe lässt sich mit dem beiliegenden gle-Code (*Graphics Layout Engine*) grafisch darstellen. Die Befehle

    gle -d pdf ht.gle
    okular ht.pdf

erzeugen die Grafik *ht.pdf* und stellen diese auf dem Bildschirm dar.

# Aufgaben
- Simulieren Sie das Spiel *The Heat is On!* und vergleichen Sie die numerischen Resultate mit denen, die Sie aus dem Spiel erhalten haben.

- Gibt es einen stationären Zustand? Falls ja, wie sieht dessen Energieverteilung aus? Versuchen Sie durch die zugehörigen Ausgabewerte eine Ausgleichsgerade zu legen.

Die letzte Zeile der Ausgabedatei können Sie mit

    tail -1 heat-transport.out > statzustand.out

in die Datei *statzustand.out* schreiben.

- Simulieren Sie den Fall, dass Bäder und der Streifen dieselbe Temperatur haben.

- Variieren Sie die Anzahl der Zeilen und die Anzahl der Spalten. Ergeben sich qualitative Änderungen der Ergebnisse?
 
