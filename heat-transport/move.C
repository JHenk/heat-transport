//
// Performing moves
//
#include <iostream>
using namespace std;
#include <iomanip>

#include <math.h>

#include "random.h"

#include "struct.h"

// Uniform distribution, integer in (0, length - 1)
int uni_dist(int length){

  return int(floor(omni_random() * length));
}


// Refill a bath
void refill_bath(bath &b){

  int i;

  double rnd;

  // Set all cells to "FILLED" or "EMPTY" randomly
  for(i = 0; i < b.width; i++){
    // Get a random number in (0,1)
    rnd = omni_random();
    if(rnd < b.filling){
      b.cells(i) = FILLED;
    }
    else {
      b.cells(i) = EMPTY;
    }
  };
}


// Perform one move
void move(ht_system &s){

  int from_row, from_column, to_row, to_column;
  int from_state, to_state;

  // Pick out a random cell
  from_row    = uni_dist(s.width);
  from_column = uni_dist(s.length);

  // Determine the swap direction from a random number in (0, 1)
  if(omni_random() < 0.5){
    // Swap with left
    to_column = from_column - 1;
  }
  else {
    // Swap with right
    to_column = from_column + 1;
  };
  if(omni_random() < 0.5){
    // Swap with bottom
    to_row = from_row - 1;
  }
  else {
    // Swap with top
    to_row = from_row + 1;
  };
  //to_row = from_row;

  // Perform a move only when the stripe is not left ...
  if((to_row >= 0) && (to_row < s.str.width)){ 
    // then ...
    if(from_column == 0){
      if(to_column == -1){
	// Swap with bath
	from_state = s.str.cells(from_row, from_column);
	to_state   = s.bath_left.cells(to_row);
	if(from_state != to_state){
	  s.str.cells(from_row, from_column) = to_state;
	  s.bath_left.cells(to_row)          = from_state;
	  s.nswaps++;
	}
      } else {
	// Swap within the stripe
	from_state = s.str.cells(from_row, from_column);
	to_state   = s.str.cells(to_row,   to_column);
	if(from_state != to_state){
	  s.str.cells(from_row, from_column) = to_state;
	  s.str.cells(to_row,   to_column)   = from_state;
	  s.nswaps++;
	}
    }
    }
    else if(from_column == s.length - 1){
      if(to_column == s.length){
	// Swap with bath
	from_state = s.str.cells(from_row, from_column);
	to_state   = s.bath_right.cells(to_row);
	if(from_state != to_state){
	  s.str.cells(from_row, from_column) = to_state;
	  s.bath_right.cells(to_row)         = from_state;
	s.nswaps++;
	}
      } else {
	// Swap within the stripe
	from_state = s.str.cells(from_row, from_column);
	to_state   = s.str.cells(to_row,   to_column);
	if(from_state != to_state){
	  s.str.cells(from_row, from_column) = to_state;
	s.str.cells(to_row,   to_column)   = from_state;
	s.nswaps++;
	}
      }
    }
    else {
      // Swap within the stripe
      from_state = s.str.cells(from_row, from_column);
      to_state   = s.str.cells(to_row,   to_column);
      if(from_state != to_state){
	s.str.cells(from_row, from_column) = to_state;
	s.str.cells(to_row,   to_column)   = from_state;
	s.nswaps++;
      }
    }
  }
}



// Cumulative average of the rows
void avg(ht_system &s, int count){

  int i, j;

  double row_sum;

  // For all columns
  for(i = 0; i < s.length; i++){
    // Row-wise sum
    row_sum = 0.0;
    for(j = 0; j < s.width; j++){
      row_sum += s.str.cells(j, i);
    }
    row_sum /= s.width;
    
    s.row_avg(i) = (count * s.row_avg(i) + row_sum) / (count + 1);
  }
  
}

