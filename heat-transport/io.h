//
// IO functions
//

// Read the job parameters
void read_job(job &j);

// Print the row-wise sums
void print_row_avg(ht_system &s, int count, ofstream &out);
