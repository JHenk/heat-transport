//
// Data structure for heat-transport
//

#include "imatrix.h"
#include "dmatrix.h"

// Cell states
#define EMPTY  0
#define FILLED 1


// Bath
struct bath{
  // Dimension
  int width;

  // Cells
  intdiagmatrix cells;

  // Filling; corresponds to temperature
  double filling;
};


// Stripe
struct stripe{
  // Dimensions of the stripe
  int width;
  int length;

  // Cells
  intmatrix cells;

  // Filling; corresponds to temperature
  double filling;
};

// Entire system
struct ht_system{
  // Dimensions of the system
  int width;
  int length;

  bath bath_left;
  bath bath_right;

  stripe str;

  doublediagmatrix row_avg;
  int              nswaps;
};


// Job parameters
struct job{
  int nsteps;
  int out_control;

  int width;
  int length;

  double filling_bath_left;
  double filling_stripe;
  double filling_bath_right;
};
