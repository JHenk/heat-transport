//
// Performing moves
//

// Refill a bath
void refill_bath(bath &b);

// Perform one move
void move(ht_system &s);

// Columnwise cumulative average
void avg(ht_system &s, int count);

