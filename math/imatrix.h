// imatrix.h
//
// deklariert Integer-Matrizen, sowie mathematische Operationen zwischen Matrizen
// und Zahlen.
//
// Hinweis: eigentlich waere es sinnvoller Matrizen und Vektoren durch
// templates zu definieren. Da templates zu dieser Zeit jedoch noch nicht
// sehr weit verbreitet sind ( zumindestens noch seltener als C++ Compiler),
// findet die Definition hier ohne templates statt. Es sollte jedoch recht
// einfach sein, die Definitionen auf templates umzustellen.
//
// Definition von DEBUG fuegt ausfuehrliche Fehlertests ein.
//

#ifndef _IMATRIX_
#define _IMATRIX_

#include <iostream>
#include <cstdlib>
#include <cstring>

#include "platform.h"

#ifndef NULL
#define NULL    0
#endif

//
// Diese Klassen werden definiert:
//

class intmatrix;
class intdiagmatrix;
// JH - Added the following lines for compatibility with gcc4.1
// Thus, the compiler option -ffriend-injection is not needed
// PB 20 June 2007
int trace(const intdiagmatrix &a);


class intsubmatrix;
class intsubdiagmatrix;

class inttempmatrix;
class inttempdiagmatrix;

//
// Definition der Matrixklasse
//

class intmatrix{
protected:
	int     	r, c;           // Anzahl Zeilen (r) und Spalten (c)
	int     	n;              // Gesamtgroesse der Matrix
	int	 	*a;             // Zeiger auf das eigentliche Feld
public:
// Die Konstruktoren
	intmatrix();
	intmatrix(int, int);
// Der Kopierkonstruktor
	intmatrix(const intmatrix &);
// Der Umkopierer
	intmatrix(const intsubmatrix &);
	intmatrix(const inttempmatrix &);
// Der Destruktor
	~intmatrix();
// Zugriff auf einzelne Matrixelemente
	int &operator()(int, int) const;
// Die Zuweisung
	intmatrix &operator=(int);
	intmatrix &operator=(const intmatrix &);
	intmatrix &operator=(const intsubmatrix &);
	intmatrix &operator=(const inttempmatrix &);
	intmatrix &operator=(const intdiagmatrix &);
// Setze die Groesse
	intmatrix &setsize(int, int);
// Test, ob Matrix benutzt ist
	friend int used(const intmatrix &);
// Alle meine Freunde
	friend class intsubmatrix;
// Unaere Operatoren
	friend inttempmatrix operator+(const intmatrix &);
	friend inttempmatrix operator-(const intmatrix &);
// Binaere Operatoren
	friend inttempmatrix operator*(const intmatrix &, const intmatrix &);
	friend inttempmatrix operator+(const intmatrix &, const intmatrix &);
	friend inttempmatrix operator-(const intmatrix &, const intmatrix &);
	friend inttempmatrix operator*(const intmatrix &, const intdiagmatrix &);
	friend inttempmatrix operator+(const intmatrix &, const intdiagmatrix &);
	friend inttempmatrix operator-(const intmatrix &, const intdiagmatrix &);
	friend inttempmatrix operator*(const intdiagmatrix &, const intmatrix &);
	friend inttempmatrix operator+(const intdiagmatrix &, const intmatrix &);
	friend inttempmatrix operator-(const intdiagmatrix &, const intmatrix &);
	friend inttempmatrix operator*(const intmatrix &, int);
	friend inttempmatrix operator+(const intmatrix &, int);
	friend inttempmatrix operator-(const intmatrix &, int);
	friend inttempmatrix operator*(int, const intmatrix &);
	friend inttempmatrix operator+(int, const intmatrix &);
	friend inttempmatrix operator-(int, const intmatrix &);
// Funktionen
	friend inttempmatrix transp(const intmatrix &);
	friend int norm(const intmatrix &);
// Ein- und Ausgabe
	friend std::ostream &operator<<(std::ostream &, const intmatrix &);
	friend std::istream &operator>>(std::istream &, const intmatrix &);
	};

//
// Definition der diagonalen Matrixklasse
//

class intdiagmatrix{
protected:
	int     	r;		// Anzahl Zeilen (r) und Spalten (r)
	int	 	*a;		// Zeiger auf das eigentliche Feld
public:
// Die Konstruktoren
	intdiagmatrix();
	intdiagmatrix(int);
// Der Kopierkonstruktor
	intdiagmatrix(const intdiagmatrix &);
// Der Umkopierer
	intdiagmatrix(const intsubdiagmatrix &);
	intdiagmatrix(const inttempdiagmatrix &);
// Der Destruktor
	~intdiagmatrix();
// Zugriff auf einzelne Matrixelemente
	int &operator()(int) const;
// Die Zuweisung
	intdiagmatrix &operator=(int);
	intdiagmatrix &operator=(const intdiagmatrix &);
	intdiagmatrix &operator=(const intsubdiagmatrix &);
	intdiagmatrix &operator=(const inttempdiagmatrix &);
// Setze die Groesse
	intdiagmatrix &setsize(int);
// Test, ob Matrix benutzt ist
	friend int used(const intdiagmatrix &);
// Alle meine Freunde
	friend class intmatrix;
	friend class intsubdiagmatrix;
// Unaere Operatoren
	friend inttempdiagmatrix operator+(const intdiagmatrix &);
	friend inttempdiagmatrix operator-(const intdiagmatrix &);
// Binaere Operatoren
	friend inttempdiagmatrix operator*(const intdiagmatrix &, const intdiagmatrix &);
	friend inttempdiagmatrix operator+(const intdiagmatrix &, const intdiagmatrix &);
	friend inttempdiagmatrix operator-(const intdiagmatrix &, const intdiagmatrix &);
	friend inttempmatrix operator*(const intmatrix &, const intdiagmatrix &);
	friend inttempmatrix operator+(const intmatrix &, const intdiagmatrix &);
	friend inttempmatrix operator-(const intmatrix &, const intdiagmatrix &);
	friend inttempmatrix operator*(const intdiagmatrix &, const intmatrix &);
	friend inttempmatrix operator+(const intdiagmatrix &, const intmatrix &);
	friend inttempmatrix operator-(const intdiagmatrix &, const intmatrix &);
	friend inttempdiagmatrix operator*(const intdiagmatrix &, int);
	friend inttempdiagmatrix operator+(const intdiagmatrix &, int);
	friend inttempdiagmatrix operator-(const intdiagmatrix &, int);
	friend inttempdiagmatrix operator*(int, const intdiagmatrix &);
	friend inttempdiagmatrix operator+(int, const intdiagmatrix &);
	friend inttempdiagmatrix operator-(int, const intdiagmatrix &);
// Funktionen
	friend inttempdiagmatrix transp(const intdiagmatrix &);
	friend int norm(const intdiagmatrix &);
        friend int trace(const intdiagmatrix &a);
// Ein- und Ausgabe
	friend std::ostream &operator<<(std::ostream &, const intdiagmatrix &);
	friend std::istream &operator>>(std::istream &, const intdiagmatrix &);
	};

//
// Definition der Sub-Matrixklasse
//

class intsubmatrix{
protected:
	int		r0, c0;		// Startzeile (r0) und Startspalte (c0)
	int     	r, c;           // Anzahl Zeilen (r) und Spalten (c)
	const intmatrix
			&ref;           // Die Referenzierte Matrix
public:
// Der Konstruktor
	intsubmatrix(const intmatrix &, int, int, int, int);
// Zugriff auf einzelne Matrixelemente
	int &operator()(int, int) const;
// Die Zuweisung
	intsubmatrix &operator=(const intmatrix &);
	intsubmatrix &operator=(const intsubmatrix &);
// Alle meine Freunde
	friend class intmatrix;
	};

//
// Definition der diagonalen Sub-Matrixklasse
//

class intsubdiagmatrix{
protected:
	int		r0;		// Startzeile (r0) und Startspalte (r0)
	int     	r;		// Anzahl Zeilen (r) und Spalten (r)
	const intdiagmatrix
			&ref;           // Die Referenzierte Matrix
public:
// Der Konstruktor
	intsubdiagmatrix(const intdiagmatrix &, int, int);
// Zugriff auf einzelne Matrixelemente
	int &operator()(int) const;
// Die Zuweisung
	intsubdiagmatrix &operator=(const intdiagmatrix &);
	intsubdiagmatrix &operator=(const intsubdiagmatrix &);
// Alle meine Freunde
	friend class intdiagmatrix;
	};

//
// Definition der temporaeren Matrixklasse
//

class inttempmatrix{
protected:
	int     	r, c;           // Anzahl Spalten (r) und Zeilen (c)
	int     	n;              // Gesamtgroesse der Matrix
	int 	*a;             // Zeiger auf das eigentliche Feld
public:
// Der Konstruktor
	inttempmatrix(int, int);
// Kein Destruktor
// Alle meine Freunde
	friend class intmatrix;
// Unaere Operatoren
	friend inttempmatrix operator+(const intmatrix &);
	friend inttempmatrix operator-(const intmatrix &);
// Binaere Operatoren
	friend inttempmatrix operator*(const intmatrix &, const intmatrix &);
	friend inttempmatrix operator+(const intmatrix &, const intmatrix &);
	friend inttempmatrix operator-(const intmatrix &, const intmatrix &);
	friend inttempmatrix operator*(const intmatrix &, const intdiagmatrix &);
	friend inttempmatrix operator+(const intmatrix &, const intdiagmatrix &);
	friend inttempmatrix operator-(const intmatrix &, const intdiagmatrix &);
	friend inttempmatrix operator*(const intdiagmatrix &, const intmatrix &);
	friend inttempmatrix operator+(const intdiagmatrix &, const intmatrix &);
	friend inttempmatrix operator-(const intdiagmatrix &, const intmatrix &);
	friend inttempmatrix operator*(const intmatrix &, int);
	friend inttempmatrix operator+(const intmatrix &, int);
	friend inttempmatrix operator-(const intmatrix &, int);
	friend inttempmatrix operator*(int, const intmatrix &);
	friend inttempmatrix operator+(int, const intmatrix &);
	friend inttempmatrix operator-(int, const intmatrix &);
// Funktionen
	friend inttempmatrix transp(const intmatrix &);
	};

//
// Definition der temporaeren diagonalen Matrixklasse
//

class inttempdiagmatrix{
protected:
	int     	r;              // Anzahl Spalten (r) und Zeilen (r)
	int 	*a;             // Zeiger auf das eigentliche Feld
public:
// Der Konstruktor
	inttempdiagmatrix(int);
// Kein Destruktor
// Alle meine Freunde
	friend class intdiagmatrix;
// Unaere Operatoren
	friend inttempdiagmatrix operator+(const intdiagmatrix &);
	friend inttempdiagmatrix operator-(const intdiagmatrix &);
// Binaere Operatoren
	friend inttempdiagmatrix operator*(const intdiagmatrix &, const intdiagmatrix &);
	friend inttempdiagmatrix operator+(const intdiagmatrix &, const intdiagmatrix &);
	friend inttempdiagmatrix operator-(const intdiagmatrix &, const intdiagmatrix &);
	friend inttempdiagmatrix operator*(const intdiagmatrix &, int);
	friend inttempdiagmatrix operator+(const intdiagmatrix &, int);
	friend inttempdiagmatrix operator-(const intdiagmatrix &, int);
	friend inttempdiagmatrix operator*(int, const intdiagmatrix &);
	friend inttempdiagmatrix operator+(int, const intdiagmatrix &);
	friend inttempdiagmatrix operator-(int, const intdiagmatrix &);
// Funktionen
	friend inttempdiagmatrix transp(const intdiagmatrix &);
	};

// ***********************************************
// * Hier folgen nun die eigentlichen Funktionen *
// ***********************************************

//
// Erzeuge Platzhalter fuer eine Matrix
//

inline  intmatrix::intmatrix()

{
// Keine Zeilen und Spalten
r = 0;
c = 0;
// Keine Groesse
n = 0;
// Kein Speicher
a = NULL;
}

//
// Erzeuge eine beliebige Matrix
//

inline  intmatrix::intmatrix(int n1, int n2)

{
#ifdef DEBUG
if(n1 <= 0 || n2 <= 0){
	cerr << "ERROR from intmatrix: Invalid dimensions\n";
	myexit(1);
	}
#endif
// Anzahl Zeilen und Spalten
r = n1;
c = n2;
// Groesse berechnen
n = r * c;
// Noetigen Speicher anfordern
a = new int[n];
}

//
// Der Kopierkonstruktor
//

inline  intmatrix::intmatrix(const intmatrix &z)

{
// Abmessungen kopieren
r = z.r;
c = z.c;
// Groesse kopieren
n = z.n;
// Speicher anfordern
a = new int[n];
// Und alles kopieren
memcpy(a, z.a, n * sizeof(int));
}

inline  intmatrix::intmatrix(const inttempmatrix &z)

{
// Abmessungen kopieren
r = z.r;
c = z.c;
// Groesse kopieren
n = z.n;
// Einfach Zeiger kopieren
a = z.a;
}

//
// Der Destruktor
//

inline  intmatrix::~intmatrix()

{
// Gibt den Speicher frei (Test auf NULL nicht noetig?)
delete[] a;
}

//
// Die Zuweisung
//

inline  intmatrix &intmatrix::operator=(const inttempmatrix &z)

{
// Gibt den Speicher frei (Test auf NULL nicht noetig?)
delete[] a;
// Abmessungen kopieren
r = z.r;
c = z.c;
// Groesse kopieren
n = z.n;
// Einfach Zeiger kopieren
a = z.a;
return *this;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline  int &intmatrix::operator()(int n1, int n2) const

{
#ifdef DEBUG
if(n1 < 0 || n1 >= r || n2 < 0 || n2 >= c){
	cerr << "ERROR from intmatrix: Subscript out of range\n";
	myexit(1);
	}
#endif
// Gib Referenz auf Element zurueck
return a[n1 * c + n2];
}

//
// Setze die Groesse einer Matrix
//

inline  intmatrix &intmatrix::setsize(int n1, int n2)

{
#ifdef DEBUG
if(n1 < 0 || n2 < 0){
	cerr << "ERROR from intmatrix: Invalid dimensions\n";
	myexit(1);
	}
#endif
if(n != n1 * n2){
	// Groesse berechnen
	n = n1 * n2;
	// Gibt den Speicher frei (Test auf NULL nicht noetig?)
	delete[] a;
	if(n != 0){
		// Noetigen Speicher anfordern
		a = new int[n];
		}
	else{
		// Keinen Speicher anfordern
		a = NULL;
		}
	}
// Anzahl Zeilen und Spalten
r = n1;
c = n2;
return *this;
}

//
// Test, ob die Matrix benutzt worden ist
//

inline int used(const intmatrix &z)

{
return z.n != 0;
}

//
// Erzeuge Platzhalter fuer eine diagonale Matrix
//

inline  intdiagmatrix::intdiagmatrix()

{
// Keine Zeilen und Spalten
r = 0;
// Kein Speicher
a = NULL;
}

//
// Erzeuge eine diagonale Matrix
//

inline  intdiagmatrix::intdiagmatrix(int n)

{
#ifdef DEBUG
if(n <= 0){
	cerr << "ERROR from intdiagmatrix: Invalid dimensions\n";
	myexit(1);
	}
#endif
// Anzahl Zeilen und Spalten
r = n;
// Noetigen Speicher anfordern
a = new int[r];
}

//
// Der Kopierkonstruktor
//

inline  intdiagmatrix::intdiagmatrix(const intdiagmatrix &z)

{
// Abmessungen kopieren
r = z.r;
// Speicher anfordern
a = new int[r];
// Und alles kopieren
memcpy(a, z.a, r * sizeof(int));
}

//
// Die Zuweisung
//

inline  intdiagmatrix::intdiagmatrix(const inttempdiagmatrix &z)

{
// Abmessungen kopieren
r = z.r;
// Einfach Zeiger kopieren
a = z.a;
}

//
// Der Destruktor
//

inline  intdiagmatrix::~intdiagmatrix()

{
// Gibt den Speicher frei (Test auf NULL nicht noetig?)
delete[] a;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline  int &intdiagmatrix::operator()(int n) const

{
#ifdef DEBUG
if(n < 0 || n >= r){
	cerr << "ERROR from intdiagmatrix: Subscript out of range\n";
	myexit(1);
	}
#endif
// Gib Referenz auf Element zurueck
return a[n];
}

//
// Die Zuweisung
//

inline  intdiagmatrix &intdiagmatrix::operator=(const inttempdiagmatrix &z)

{
// Gibt den Speicher frei (Test auf NULL nicht noetig?)
delete[] a;
// Abmessungen kopieren
r = z.r;
// Einfach Zeiger kopieren
a = z.a;
return *this;
}

//
// Setze die Groesse einer diagonalen Matrix
//

inline  intdiagmatrix &intdiagmatrix::setsize(int n)

{
#ifdef DEBUG
if(n < 0){
	cerr << "ERROR from intdiagmatrix: Invalid dimensions\n";
	myexit(1);
	}
#endif
if(r != n){
	// Anzahl Zeilen und Spalten
	r = n;
	// Gibt den Speicher frei (Test auf NULL nicht noetig?)
	delete[] a;
	if(n != 0){
		// Noetigen Speicher anfordern
		a = new int[r];
		}
	else{
		// Keinen Speicher anfordern
		a = NULL;
		}
	}
return *this;
}

//
// Test, ob die Matrix benutzt worden ist
//

inline int used(const intdiagmatrix &z)

{
return z.r != 0;
}

//
// Erzeuge eine Submatrix
//

inline	intsubmatrix::intsubmatrix(const intmatrix &z, int m1, int m2, int n1, int n2) : ref(z)

{
#ifdef DEBUG
if(m1 < 0 || m2 < 0 || n1 <= 0 || n2 <= 0){
	cerr << "ERROR from intsubmatrix: Invalid subdimensions\n";
	myexit(1);
	}
#endif
// Der Startpunkt
r0 = m1;
c0 = m2;
// Die Groesse
r = n1;
c = n2;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline	int &intsubmatrix::operator()(int n1, int n2) const

{
#ifdef DEBUG
if(n1 < 0 || n1 >= r || n2 < 0 || n2 >= c){
	cerr << "ERROR from intsubmatrix: Subscript out of range\n";
	myexit(1);
	}
#endif
// Gib Referenz auf Element zurueck
return ref(n1 + r0, n2 + c0);
}

//
// Erzeuge eine diagonale Submatrix
//

inline	intsubdiagmatrix::intsubdiagmatrix(const intdiagmatrix &z, int m, int n) : ref(z)

{
#ifdef DEBUG
if(m < 0 || n <= 0){
	cerr << "ERROR from intsubdiagmatrix: Invalid subdimensions\n";
	myexit(1);
	}
#endif
// Der Startpunkt
r0 = m;
// Die Groesse
r = n;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline	int &intsubdiagmatrix::operator()(int n) const

{
#ifdef DEBUG
if(n < 0 || n >= r){
	cerr << "ERROR from intsubdiagmatrix: Subscript out of range\n";
	myexit(1);
	}
#endif
// Gib Referenz auf Element zurueck
return ref(n + r0);
}

//
// Erzeuge eine temporaere Matrix
//

inline  inttempmatrix::inttempmatrix(int n1, int n2)

{
// Anzahl Zeilen und Spalten
r = n1;
c = n2;
// Groesse berechnen
n = n1 * n2;
// Noetigen Speicher anfordern
a = new int[n];
}

//
// Erzeuge eine temporaere diagonale Matrix
//

inline  inttempdiagmatrix::inttempdiagmatrix(int n)

{
// Anzahl Zeilen und Spalten
r = n;
// Noetigen Speicher anfordern
a = new int[r];
}

#endif
