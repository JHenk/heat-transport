//
// Distributions
//
#include <cmath>

#include "platform.h"
#include "constants.h"
#include "vector3.h"
#include "complex.h"

using namespace std;

// Gaussian distribution, with mean mue and variance sigma^{2}
double gaussian(double x, double mue, double sigma){

  return exp(- (x - mue) * (x - mue) / 2 / sigma / sigma) / sqrt(2 * M_PI) / sigma;

}


// Cauchy distribution, with mean mue and half-width-at-half-maximum gamma
// (also known as Lorentzian)
double cauchy(double x, double mue, double gamma){

  return gamma / M_PI / ((x - mue) * (x - mue) + gamma * gamma);

}


// Von Mises-Fisher distribution for 3dimensional space (p = 3)
// width:      1/kappa
// direction:  theta_0, phi_0 (on the unit sphere, in radians)
double vonMisesFisher3(double kappa, double theta_0, double phi_0, double theta, double phi){

  double e3, f3;

  vector3 mue, x;

  // Initialize
  f3 = 0;

  if(kappa < 0){
    // Invalid number
    cerr << "ERROR from vonMisesFisher3(): kappa negative. " << kappa << endl;
    myexit(1);
  }
  else if(kappa == 0.0){
    // Homogeneous distribution
    f3 = 1 / 4.0 / M_PI;
  }
  else{
    // Directed distribution
    // -- Scalar product
    mue = vector3(cos(phi_0) * sin(theta_0), sin(phi_0) * sin(theta_0), cos(theta_0));
    x   = vector3(cos(phi)   * sin(theta),   sin(phi)   * sin(theta),   cos(theta));
    e3  = mue * x;
    
    // -- Distribution function
    f3 = kappa / 2.0 / M_PI / (exp(kappa * (1.0 - e3)) - exp(-kappa * (1.0 + e3)));
  }

  return f3;

}

// JH Original, suffers from overflow for large kappa
// double vonMisesFisher3(double kappa, double theta_0, double phi_0, double theta, double phi){

//   double c3, e3, f3;

//   vector3 mue, x;

//   // Initialize
//   f3 = 0;

//   if(kappa < 0){
//     // Invalid number
//     cerr << "ERROR from vonMisesFisher3(): kappa negative. " << kappa << endl;
//     myexit(1);
//   }
//   else if(kappa == 0.0){
//     // Homogeneous distribution
//     f3 = 1 / 4.0 / M_PI;
//   }
//   else{
//     // Directed distribution
//     // -- Normalization constant
//     c3 = kappa / (exp(kappa) - exp(-kappa)) / 2.0 / M_PI;
    
//     // -- Exponential factor
//     mue = vector3(cos(phi_0) * sin(theta_0), sin(phi_0) * sin(theta_0), cos(theta_0));
//     x   = vector3(cos(phi)   * sin(theta),   sin(phi)   * sin(theta),   cos(theta));
//     e3  = exp(kappa * (mue * x));
    
//     // -- Distribution function
//     f3 = e3 * c3;
//   }

//   return f3;

// }


// Fermi-Dirac distribution, in the complex energy plane
// e   - complex energy
// mue - Fermi level
// kt  - thermal energy
complex FermiDirac(complex e, double mue, double kt){
  
  complex fd;
  
  fd = cmplx(0.0, 0.0);
  
  // Error checking
  if(kt < 0.0){
    cerr << "ERROR from FermiDirac(): temperature is negative. " << kt << endl;
    myexit(-1);
  }
  
  if(fabs(kt) < EMACH){
    // T = 0
    if(e.re < mue){
      fd = 1.0;
    }
    else if(fabs(e.re - mue) < EMACH){
      fd = 0.5;
    }
    else if(e.re > mue){
      fd = 0.0;
    }
  }
  else{
    // T > 0
    if(e.re <= mue){
      fd = 1.0 / (1.0 + exp((e - mue) / kt));
    }
    else{
      // f(x) = 1 - f(-x)
      fd = exp(-(e - mue) / kt) / (1.0 + exp(-(e - mue) / kt));
    }
  }
  
  return fd;
  
}

// Bose-Einstein distribution, in the complex energy plane
// e   - complex energy
// mue - Fermi level
// kt  - thermal energy
complex BoseEinstein(complex e, double mue, double kt){
  
  // Error checking
  if(kt <= 0.0){
    cerr << "ERROR from BoseEinstein(): temperature is not positive. " << kt << endl;
    myexit(-1);
  }
  
  if(e.re <= mue){
    cerr << "ERROR from BoseEinstein(): energy less or equal chemical potential. " << e << " " << mue << endl;
    myexit(-1);
  }

  return (1.0 / (exp((e - mue) / kt) - 1.0));
  
}

