// table.h
//
// enthaelt die Definition der reellen und komplexen Tabellenklassen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _TABLE_
#define _TABLE_

#include <iostream> 
#include <cstdlib>
#include <cstring>

#include "complex.h"
#include "platform.h"

#ifndef NULL
#define NULL	0
#endif

//
// Definition der rellen Tabellenklasse
//

class doubletable{
protected:
	int		n;		// Anzahl Eintraege
	double		*xv;		// Speicher fuer die x-Werte
	double		*yv;		// Speicher fuer die y-Werte
public:
// Die Konstruktoren
	doubletable();
	doubletable(int);
	// Copy constructor
        doubletable(const doubletable &); 
// Der Destruktor
	~doubletable();
// Zugriff auf die Elemente
	double &x(int) const;
	double &y(int) const;
// Setze die Zahl der Stuetzstellen
	doubletable &setsize(int);
// Interpolationsoperator
	double operator()(double);
// Ein- und Ausgabe
	friend std::ostream &operator<<(std::ostream &, const doubletable &);
	friend std::istream &operator>>(std::istream &, const doubletable &);
	};

//
// Definition der komplexen Tabellenklasse
//

class complextable{
protected:
	int		n;		// Anzahl Eintraege
	double		*xv;		// Speicher fuer die x-Werte
	complex		*yv;		// Speicher fuer die y-Werte
public:
// Die Konstruktoren
	complextable();
	complextable(int);
// Der Destruktor
	~complextable();
// Zugriff auf die Elemente
	double &x(int) const;
	complex &y(int) const;
// Setze die Zahl der Stuetzstellen
	complextable &setsize(int);
// Interpolationsoperator
	complex operator()(double);
// Ein- und Ausgabe
	friend std::ostream &operator<<(std::ostream &, const complextable &);
	friend std::istream &operator>>(std::istream &, const complextable &);
	};

// ***********************************************
// * Hier folgen nun die eigentlichen Funktionen *
// ***********************************************

//
// Erzeuge Platzhalter fuer eine reelle Tabelle
//

inline	doubletable::doubletable()

{
// Keine Stuetzstellen
n = 0;
// Kein Speicher
xv = NULL;
yv = NULL;
}

//
// Erzeuge eine reelle Tabelle
//

inline	doubletable::doubletable(int nn)

{
#ifdef DEBUG
if(nn <= 0){
	cerr << "ERROR from doubletable: Invalid number\n";
	myexit(1);
	}
#endif
// Anzahl Stuetzstellen
n = nn;
// Noetigen Speicher anfordern
xv = new double[n];
yv = new double[n];
}

//
// Der Destruktor
//

inline	doubletable::~doubletable()

{
// Gibt den Speicher frei
delete[] xv;
delete[] yv;
}

//
// Zugriff auf einzelne Stuetzstellen
//

inline	double &doubletable::x(int nn) const

{
#ifdef DEBUG
if(nn < 0 || nn >= n){
	cerr << "ERROR from doubletable: Number out of range\n";
	myexit(1);
	}
#endif
// Gibt Referenz auf Element zurueck
return xv[nn];
}

inline	double &doubletable::y(int nn) const

{
#ifdef DEBUG
if(nn < 0 || nn >= n){
	cerr << "ERROR from doubletable: Number out of range\n";
	myexit(1);
	}
#endif
// Gibt Referenz auf Element zurueck
return yv[nn];
}

//
// Setze Zahl der Stuetzstellen
//

inline	doubletable &doubletable::setsize(int nn)

{
#ifdef DEBUG
if(nn <= 0){
	cerr << "ERROR from doubletable: invalid number\n";
	myexit(1);
	}
#endif
if(n != nn){
	// Anzahl Stuetzstellen
	n = nn;
	// Gib den Speicher frei
	delete[] xv;
	// Noetigen Speicher anfordern
	xv = new double[n];
	// Gib den Speicher frei
	delete[] yv;
	// Noetigen Speicher anfordern
	yv = new double[n];
	}
return *this;
}


// Copy constructor
inline  doubletable::doubletable(const doubletable &z){
  int i;

  // Anzahl Stuetzstellen
  n = z.n;

  delete[] xv;
  delete[] yv;

  // Speicher anfordern
  xv = new double[n];
  yv = new double[n];

  // Und alles kopieren
  for(i = 0; i < n; i++){
    xv[i] = z.xv[i];
    yv[i] = z.yv[i];
  }
}



//
// Erzeuge Platzhalter fuer eine komplexe Tabelle
//

inline	complextable::complextable()

{
// Keine Stuetzstellen
n = 0;
// Kein Speicher
xv = NULL;
yv = NULL;
}

//
// Erzeuge eine komplexe Tabelle
//

inline	complextable::complextable(int nn)

{
#ifdef DEBUG
if(nn <= 0){
	cerr << "ERROR from complextable: Invalid number\n";
	myexit(1);
	}
#endif
// Anzahl Stuetzstellen
n = nn;
// Noetigen Speicher anfordern
xv = new double[n];
yv = new complex[n];
}

//
// Der Destruktor
//

inline	complextable::~complextable()

{
// Gibt den Speicher frei
delete[] xv;
delete[] yv;
}

//
// Zugriff auf einzelne Stuetzstellen
//

inline	double &complextable::x(int nn) const

{
#ifdef DEBUG
if(nn < 0 || nn >= n){
	cerr << "ERROR from complextable: Number out of range\n";
	myexit(1);
	}
#endif
// Gibt Referenz auf Element zurueck
return xv[nn];
}

inline	complex &complextable::y(int nn) const

{
#ifdef DEBUG
if(nn < 0 || nn >= n){
	cerr << "ERROR from complextable: Number out of range\n";
	myexit(1);
	}
#endif
// Gibt Referenz auf Element zurueck
return yv[nn];
}

//
// Setze Zahl der Stuetzstellen
//

inline	complextable &complextable::setsize(int nn)

{
#ifdef DEBUG
if(nn <= 0){
	cerr << "ERROR from complextable: Invalid number\n";
	myexit(1);
	}
#endif
if(n != nn){
	// Anzahl Stuetzstellen
	n = nn;
	// Gib den Speicher frei
	delete[] xv;
	// Noetigen Speicher anfordern
	xv = new double[n];
	// Gib den Speicher frei
	delete[] yv;
	// Noetigen Speicher anfordern
	yv = new complex[n];
	}
return *this;
}


#endif
