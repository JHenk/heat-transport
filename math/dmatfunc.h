// dmatfunc.h
//
// enthaelt die Definition der reellen Matrix-Funktionenklassen
//
// Autor: 1999 Dipl. Phys. Thomas Scheunemann
//        Gerhard Mercator Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _DMATFUNC_
#define _DMATFUNC_

#include <iostream>
#include <cstdlib>
#include <cstring>

#include "platform.h"

#include "dmatrix.h"

#ifndef NULL
#define NULL	0
#endif

//
// Diese Klassen werden hier definiert
//

class doublematrixfunction;
class doubletempmatrixfunction;

class doublediagmatrixfunction;
class doubletempdiagmatrixfunction;

//
// Diese Klassen werden hier nicht definiert
//

class complex;

class doublefunction;

class complexmatrix;
class complexdiagmatrix;

class complexmatrixfunction;
class complextempmatrixfunction;

class complexdiagmatrixfunction;
class complextempdiagmatrixfunction;

//
// Definition der rellen Matrix-Funktionenklasse
//

class doublematrixfunction{
 protected:
  int		n;		// Anzahl Stuetzstellen
  doublematrix	*v;		// Speicher fuer die Stuetzstellen
 public:
  // Die Konstruktoren
  doublematrixfunction();
  doublematrixfunction(int);
  doublematrixfunction(int, int, int);
  // Der Kopierkonstruktor
  doublematrixfunction(const doublematrixfunction &);
  // Der Umkopierer
  doublematrixfunction(const doubletempmatrixfunction &);
  // Der Destruktor
  ~doublematrixfunction();
  // Zugriff auf einzelne Stuetzstellen
  doublematrix &operator()(int) const;
  doubletempfunction f(int, int) const;
  // Die Zuweisung
  doublematrixfunction &operator=(double);
  doublematrixfunction &operator=(const doublematrix &);
  doublematrixfunction &operator=(const doublematrixfunction &);
  doublematrixfunction &operator=(const doubletempmatrixfunction &);
  // Setze die Zahl der Stuetzstellen
  doublematrixfunction &setsize(int);
  doublematrixfunction &setsize(int, int, int);
  // Test, ob die Funktion benutzt ist
  friend int used(const doublematrixfunction &);
  // Alle meine Freunde
  friend class complexmatrixfunction;
  // Unaere Operatoren
  friend doubletempmatrixfunction operator+(const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &);
  // Binaere Operatoren
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator*(double, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(double, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(double, const doublematrixfunction &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, double);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, double);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, double);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complex &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complex &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complex &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complex &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complex &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complex &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublediagmatrix &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublediagmatrix &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublediagmatrix &);
  friend doubletempmatrixfunction operator*(const doublediagmatrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublediagmatrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublediagmatrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator*(const doublematrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublematrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublematrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator*(const complexdiagmatrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexdiagmatrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexdiagmatrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator*(const doublediagmatrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublediagmatrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublediagmatrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublefunction &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublefunction &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublefunction &);
  friend doubletempmatrixfunction operator*(const doublefunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublefunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublefunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexdiagmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexdiagmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexdiagmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator*(const complexfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexfunction &, const doublematrixfunction &);
  // Funktionen
  friend doubletempmatrixfunction inv(const doublematrixfunction &);
  friend doubletempmatrixfunction transp(const doublematrixfunction &);
  friend doubletempmatrixfunction integral(const doublematrixfunction &, const doublematrix &);
  friend doublematrix integral(const doublematrixfunction &);
  // Ein- und Ausgabe
  friend std::ostream &operator<<(std::ostream &, const doublematrixfunction &);
  friend std::istream &operator>>(std::istream &, const doublematrixfunction &);
};

//
// Definition der rellen diagonalen Matrix-Funktionenklasse
//

class doublediagmatrixfunction{
 protected:
  int		n;		// Anzahl Stuetzstellen
  doublediagmatrix
    *v;		// Speicher fuer die Stuetzstellen
 public:
  // Die Konstruktoren
  doublediagmatrixfunction();
  doublediagmatrixfunction(int);
  doublediagmatrixfunction(int, int);
  // Der Kopierkonstruktor
  doublediagmatrixfunction(const doublediagmatrixfunction &);
  // Der Umkopierer
  doublediagmatrixfunction(const doubletempdiagmatrixfunction &);
  // Der Destruktor
  ~doublediagmatrixfunction();
  // Zugriff auf einzelne Stuetzstellen
  doublediagmatrix &operator()(int) const;
  doubletempfunction f(int) const;
  // Die Zuweisung
  doublediagmatrixfunction &operator=(double);
  doublediagmatrixfunction &operator=(const doublediagmatrix &);
  doublediagmatrixfunction &operator=(const doublediagmatrixfunction &);
  doublediagmatrixfunction &operator=(const doubletempdiagmatrixfunction &);
  // Setze die Zahl der Stuetzstellen
  doublediagmatrixfunction &setsize(int);
  doublediagmatrixfunction &setsize(int, int);
  // Test, ob die Funktion benutzt ist
  friend int used(const doublediagmatrixfunction &);
  // Alle meine Freunde
  friend class complexdiagmatrixfunction;
  // Unaere Operatoren
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &);
  // Binaere Operatoren
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator*(double, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator+(double, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator-(double, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &, double);
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &, double);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &, double);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complex &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complex &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complex &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const complex &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const complex &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const complex &);
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const doublediagmatrix &);
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const doublediagmatrix &);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const doublediagmatrix &);
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrix &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrix &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrix &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator*(const doublediagmatrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator+(const doublediagmatrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator-(const doublediagmatrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator*(const doublematrix &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator+(const doublematrix &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator-(const doublematrix &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrix &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrix &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrix &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublediagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator+(const doublediagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator-(const doublediagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrix &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrix &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator*(const doublediagmatrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublediagmatrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublediagmatrixfunction &, const doublematrixfunction &);
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const doublefunction &);
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const doublefunction &);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const doublefunction &);
  friend doubletempdiagmatrixfunction operator*(const doublefunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator+(const doublefunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator-(const doublefunction &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublediagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublediagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublediagmatrixfunction &, const complexmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator*(const complexfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexfunction &, const doublediagmatrixfunction &);
  // Funktionen
  friend doubletempdiagmatrixfunction inv(const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction transp(const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction integral(const doublediagmatrixfunction &, const doublediagmatrix &);
  friend doublediagmatrix integral(const doublediagmatrixfunction &);
  // Ein- und Ausgabe
  friend std::ostream &operator<<(std::ostream &, const doublediagmatrixfunction &);
  friend std::istream &operator>>(std::istream &, const doublediagmatrixfunction &);
};

//
// Definition der temporaeren reellen Matrix-Funktionenklasse
//

class doubletempmatrixfunction{
 protected:
  int		n;		// Anzahl Stuetzstellen
  doublematrix	*v;		// Speicher fuer die Stuetzstellen
 public:
  // Der Konstruktor
  doubletempmatrixfunction(int);
  doubletempmatrixfunction(int, int, int);
  // Kein Destruktor
  // Alle meine Freunde
  friend class doublematrixfunction;
  // Unaere Operatoren
  friend doubletempmatrixfunction operator+(const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &);
  // Binaere Operatoren
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator*(double, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(double, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(double, const doublematrixfunction &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, double);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, double);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, double);
  friend doubletempmatrixfunction operator*(const doublefunction &, const doublematrix &);
  friend doubletempmatrixfunction operator*(const doublematrix &, const doublefunction &);
  friend doubletempmatrixfunction operator*(const doublediagmatrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator+(const doublediagmatrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator-(const doublediagmatrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator*(const doublematrix &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator+(const doublematrix &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator-(const doublematrix &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublediagmatrix &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublediagmatrix &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublediagmatrix &);
  friend doubletempmatrixfunction operator*(const doublediagmatrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublediagmatrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublediagmatrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublematrix &);
  friend doubletempmatrixfunction operator*(const doublematrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublematrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublematrix &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublediagmatrixfunction &);
  friend doubletempmatrixfunction operator*(const doublediagmatrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublediagmatrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublediagmatrixfunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublefunction &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublefunction &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublefunction &);
  friend doubletempmatrixfunction operator*(const doublefunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublefunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublefunction &, const doublematrixfunction &);
  // Funktionen
  friend doubletempmatrixfunction inv(const doublematrixfunction &);
  friend doubletempmatrixfunction transp(const doublematrixfunction &);
  friend doubletempmatrixfunction integral(const doublematrixfunction &, const doublematrix &);
};

//
// Definition der temporaeren reellen diagonalen Matrix-Funktionenklasse
//

class doubletempdiagmatrixfunction{
 protected:
  int		n;		// Anzahl Stuetzstellen
  doublediagmatrix
    *v;		// Speicher fuer die Stuetzstellen
 public:
  // Der Konstruktor
  doubletempdiagmatrixfunction(int);
  doubletempdiagmatrixfunction(int, int);
  // Kein Destruktor
  // Alle meine Freunde
  friend class doublediagmatrixfunction;
  // Unaere Operatoren
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &);
  // Binaere Operatoren
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator*(double, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator+(double, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator-(double, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &, double);
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &, double);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &, double);
  friend doubletempdiagmatrixfunction operator*(const doublefunction &, const doublediagmatrix &);
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrix &, const doublefunction &);
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const doublediagmatrix &);
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const doublediagmatrix &);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const doublediagmatrix &);
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrix &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrix &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrix &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const doublefunction &);
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const doublefunction &);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const doublefunction &);
  friend doubletempdiagmatrixfunction operator*(const doublefunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator+(const doublefunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator-(const doublefunction &, const doublediagmatrixfunction &);
  // Funktionen
  friend doubletempdiagmatrixfunction inv(const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction transp(const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction integral(const doublediagmatrixfunction &, const doublediagmatrix &);
};

// ***********************************************
// * Hier folgen nun die eigentlichen Funktionen *
// ***********************************************

//
// Erzeuge Platzhalter fuer eine reelle Matrix-Funktion
//

inline	doublematrixfunction::doublematrixfunction()

{
  // Keine Stuetzstellen
  n = 0;
  // Kein Speicher
  v = NULL;
}

//
// Erzeuge eine reelle Matrix-Funktion
//

inline	doublematrixfunction::doublematrixfunction(int nn)

{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from doublematrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new doublematrix[n];
}

inline	doublematrixfunction::doublematrixfunction(int nn, int rr, int cc)

{
#ifdef DEBUG
  if(nn <= 0 || rr <= 0 || cc <= 0){
    cerr << "ERROR from doublematrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  //#ifdef GCC3
  //v = new doublematrix[n](rr, cc);
  //#elif defined(GCC4) || defined(SGI)
  v = new doublematrix[n];

  int i;
  for(i = 0; i < n; i++){
    v[i] = doublematrix(rr, cc);
  }
  //#endif
  
}

//
// Der Kopierkonstruktor
//

inline	doublematrixfunction::doublematrixfunction(const doublematrixfunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Speicher anfordern
  v = new doublematrix[n];
  // Und alles kopieren
  int i = n;
  doublematrix *pm = v;
  doublematrix *pz = z.v;
  while( --i >= 0){
    if(used(*pz)){
      *pm = *pz;
    }
    pz += 1;
    pm += 1;
  }
}

//
// Der Umkopierer
//

inline	doublematrixfunction::doublematrixfunction(const doubletempmatrixfunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Einfach Zeiger kopieren
  v = z.v;
}

//
// Der Destruktor
//

inline	doublematrixfunction::~doublematrixfunction()

{
  // Gibt den Speicher frei
  delete[] v;
}

//
// Zugriff auf einzelne Stuetzstellen
//

inline	doublematrix &doublematrixfunction::operator()(int nn) const

{
#ifdef DEBUG
  if(nn < 0 || nn >= n){
    cerr << "ERROR from doublematrix: Number out of range\n";
    myexit(1);
  }
#endif
  // Gibt Referenz auf Element zurueck
  return v[nn];
}

inline doubletempfunction doublematrixfunction::f(int rr, int cc) const

{
  doubletempfunction f(n);

  double *p = f.v;
  doublematrix *pp = v;

  int i = n;
  while(--i >= 0){
    *p++ = (*pp++)(rr, cc);
  }
  return f;
}

//
// Die Zuweisung
//

inline	doublematrixfunction &doublematrixfunction::operator=(double z)

{
  doublematrix *pv = v;
  int i = n;
  while(--i >= 0){
    *pv++ = z;
  }
  return *this;
}

  inline	doublematrixfunction &doublematrixfunction::operator=(const doublematrix &z)

  {
    doublematrix *pv = v;
    int i = n;
    while(--i >= 0){
      *pv++ = z;
    }
    return *this;
  }

    inline	doublematrixfunction &doublematrixfunction::operator=(const doublematrixfunction &z)

    {
      if(n != z.n){
	// Anzahl kopieren
	n = z.n;
	// Gibt den Speicher frei
	delete[] v;
	// Speicher anfordern
	v = new doublematrix[n];
      }
      // Und alles kopieren
      int i = n;
      doublematrix *pm = v;
      doublematrix *pz = z.v;
      while( --i >= 0){
	if(used(*pz)){
	  *pm = *pz;
	}
	pz += 1;
	pm += 1;
      }
      return *this;
    }

      inline	doublematrixfunction &doublematrixfunction::operator=(const doubletempmatrixfunction &z)

      {
	// Gibt den Speicher frei
	delete[] v;
	// Anzahl kopieren
	n = z.n;
	// Einfach Zeiger kopieren
	v = z.v;
	return *this;
      }

      //
      // Setze Zahl der Stuetzstellen
      //

	inline	doublematrixfunction &doublematrixfunction::setsize(int nn)

	{
#ifdef DEBUG
	  if(nn <= 0){
	    cerr << "ERROR from doublematrixfunction: Invalid number\n";
	    myexit(1);
	  }
#endif
	  if(n != nn){
	    // Anzahl Stuetzstellen
	    n = nn;
	    // Gib den Speicher frei
	    delete[] v;
	    if(n != 0){
	      // Noetigen Speicher anfordern
	      v = new doublematrix[n];
	    }
	    else{
	      // Keinen Speicher anfordern
	      v = NULL;
	    }
	  }
	  return *this;
	}

inline	doublematrixfunction &doublematrixfunction::setsize(int nn, int rr, int cc)

{
#ifdef DEBUG
  if(nn <= 0 || rr <= 0 || cc <= 0){
    cerr << "ERROR from doublematrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Gib den Speicher frei
  delete[] v;
  if(n != 0){
    // Noetigen Speicher anfordern
    //#ifdef GCC3
    //v = new doublematrix[n](rr, cc);
    //#elif defined(GCC4) || defined(SGI)
    v = new doublematrix[n];

    int i;
    for(i = 0; i < n; i++){
      v[i] = doublematrix(rr, cc);
    }
    //#endif
  }
  else{
    // Keinen Speicher anfordern
    v = NULL;
  }
  return *this;
}

//
// Test, ob die Funktion benutzt worden ist
//

inline int used(const doublematrixfunction &z)

{
  return z.n != 0;
}

//
// Erzeuge Platzhalter fuer eine reelle diagonale Matrix-Funktion
//

inline	doublediagmatrixfunction::doublediagmatrixfunction()

{
  // Keine Stuetzstellen
  n = 0;
  // Kein Speicher
  v = NULL;
}

//
// Erzeuge eine reelle diagonale Matrix-Funktion
//

inline	doublediagmatrixfunction::doublediagmatrixfunction(int nn)

{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from doublediagmatrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new doublediagmatrix[n];
}

inline	doublediagmatrixfunction::doublediagmatrixfunction(int nn, int rr)

{
#ifdef DEBUG
  if(nn <= 0 || rr <= 0){
    cerr << "ERROR from doublediagmatrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  //#ifdef GCC3
  //v = new doublediagmatrix[n](rr);
  //#elif defined(GCC4) || defined(SGI)
  v = new doublediagmatrix[n];

  int i;
  for(i = 0; i < n; i++){
    v[i] = doublediagmatrix(rr);
  }
  //#endif
}

//
// Der Kopierkonstruktor
//

inline	doublediagmatrixfunction::doublediagmatrixfunction(const doublediagmatrixfunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Speicher anfordern
  v = new doublediagmatrix[n];
  // Und alles kopieren
  int i = n;
  doublediagmatrix *pm = v;
  doublediagmatrix *pz = z.v;
  while( --i >= 0){
    if(used(*pz)){
      *pm = *pz;
    }
    pz += 1;
    pm += 1;
  }
}

//
// Der Umkopierer
//

inline	doublediagmatrixfunction::doublediagmatrixfunction(const doubletempdiagmatrixfunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Einfach Zeiger kopieren
  v = z.v;
}

//
// Der Destruktor
//

inline	doublediagmatrixfunction::~doublediagmatrixfunction()

{
  // Gibt den Speicher frei
  delete[] v;
}

//
// Zugriff auf einzelne Stuetzstellen
//

inline	doublediagmatrix &doublediagmatrixfunction::operator()(int nn) const

{
#ifdef DEBUG
  if(nn < 0 || nn >= n){
    cerr << "ERROR from doublediagmatrixfunction: Number out of range\n";
    myexit(1);
  }
#endif
  // Gibt Referenz auf Element zurueck
  return v[nn];
}

inline doubletempfunction doublediagmatrixfunction::f(int rr) const

{
  doubletempfunction f(n);

  double *p = f.v;
  doublediagmatrix *pp = v;

  int i = n;
  while(--i >= 0){
    *p++ = (*pp++)(rr);
  }
  return f;
}

//
// Die Zuweisung
//

inline	doublediagmatrixfunction &doublediagmatrixfunction::operator=(double z)

{
  doublediagmatrix *pv = v;
  int i = n;
  while(--i >= 0){
    *pv++ = z;
  }
  return *this;
}

  inline	doublediagmatrixfunction &doublediagmatrixfunction::operator=(const doublediagmatrix &z)

  {
    doublediagmatrix *pv = v;
    int i = n;
    while(--i >= 0){
      *pv++ = z;
    }
    return *this;
  }

    inline	doublediagmatrixfunction &doublediagmatrixfunction::operator=(const doublediagmatrixfunction &z)

    {
      if(n != z.n){
	// Anzahl kopieren
	n = z.n;
	// Gibt den Speicher frei
	delete[] v;
	// Speicher anfordern
	v = new doublediagmatrix[n];
      }
      // Und alles kopieren
      int i = n;
      doublediagmatrix *pm = v;
      doublediagmatrix *pz = z.v;
      while( --i >= 0){
	if(used(*pz)){
	  *pm = *pz;
	}
	pz += 1;
	pm += 1;
      }
      return *this;
    }

      inline	doublediagmatrixfunction &doublediagmatrixfunction::operator=(const doubletempdiagmatrixfunction &z)

      {
	// Gibt den Speicher frei
	delete[] v;
	// Anzahl kopieren
	n = z.n;
	// Einfach Zeiger kopieren
	v = z.v;
	return *this;
      }

      //
      // Setze Zahl der Stuetzstellen
      //

	inline	doublediagmatrixfunction &doublediagmatrixfunction::setsize(int nn)

	{
#ifdef DEBUG
	  if(nn <= 0){
	    cerr << "ERROR from doublediagmatrixfunction: Invalid number\n";
	    myexit(1);
	  }
#endif
	  if(n != nn){
	    // Anzahl Stuetzstellen
	    n = nn;
	    // Gib den Speicher frei
	    delete[] v;
	    if(n != 0){
	      // Noetigen Speicher anfordern
	      v = new doublediagmatrix[n];
	    }
	    else{
	      // Keinen Speicher anfordern
	      v = NULL;
	    }
	  }
	  return *this;
	}

inline	doublediagmatrixfunction &doublediagmatrixfunction::setsize(int nn, int rr)

{
#ifdef DEBUG
  if(nn <= 0 || rr <= 0){
    cerr << "ERROR from doublediagmatrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Gib den Speicher frei
  delete[] v;
  if(n != 0){
    // Noetigen Speicher anfordern
    //#ifdef GCC3
    //v = new doublediagmatrix[n](rr);
    //#elif defined(GCC4) || defined(SGI)
    v = new doublediagmatrix[n];

    int i;
    for(i = 0; i < n; i++){
      v[i] = doublediagmatrix(rr);
    }
    //#endif
  }
  else{
    // Keinen Speicher anfordern
    v = NULL;
  }
  return *this;
}

//
// Test, ob die Funktion benutzt worden ist
//

inline int used(const doublediagmatrixfunction &z)

{
  return z.n != 0;
}

//
// Erzeuge eine temporaere reelle Matrix-Funktion
//

inline	doubletempmatrixfunction::doubletempmatrixfunction(int nn)

{
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new doublematrix[n];
}

inline	doubletempmatrixfunction::doubletempmatrixfunction(int nn, int rr, int cc)

{
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  //#ifdef GCC3
  //v = new doublematrix[n](rr, cc);
  //#elif defined(GCC4) || defined(SGI)
  v = new doublematrix[n];

  int i;
  for(i = 0; i < n; i++){
    v[i] = doublematrix(rr, cc);
  }
  //#endif
}

//
// Erzeuge eine temporaere reelle diagonale Matrix-Funktion
//

inline	doubletempdiagmatrixfunction::doubletempdiagmatrixfunction(int nn)

{
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new doublediagmatrix[n];
}

inline	doubletempdiagmatrixfunction::doubletempdiagmatrixfunction(int nn, int rr)

{
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  //#ifdef GCC3
  //v = new doublediagmatrix[n](rr);
  //#elif defined(GCC4) || defined(SGI)
  v = new doublediagmatrix[n];

  int i;
  for(i = 0; i < n; i++){
    v[i] = doublediagmatrix(rr);
  }
  //#endif
}

#endif
