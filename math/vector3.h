// vector3.h
//
// deklariert relle dreidimensionale Vektoren, sowie mathematische Operationen
// zwischen Vektoren und reellen Zahlen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _VECTOR3_
#define _VECTOR3_

#include <iostream>
#include <cmath>

#include "constants.h"

#include "platform.h"

#include "vector2.h"

class vector3: public vector2{
 public:
  double		z;		// z component of the 3D vector
  // Constructors
  vector3();
  vector3(double, double, double);
  // Destructor
  ~vector3();
  // Assignments
  vector3 &operator=(double);
  vector3 &operator=(const vector3 &);
  // Unary operator
  inline friend vector3 operator+(const vector3 &);
  inline friend vector3 operator-(const vector3 &);
  // Binary operator
  inline friend double operator*(const vector3 &, const vector3 &);
  inline friend vector3 operator+(const vector3 &, const vector3 &);
  inline friend vector3 operator-(const vector3 &, const vector3 &);
  inline friend vector3 operator*(const vector3 &, double);
  inline friend vector3 operator/(const vector3 &, double);
  inline friend vector3 operator*(double, const vector3 &);
  // Functions
  inline friend double norm(const vector3 &);
  inline friend vector3 cross(const vector3 &a, const vector3 &b);
  inline friend double polar3(const vector3 &);
  inline friend double azimuth3(const vector3 &);
  // IO
  inline friend std::ostream &operator<<(std::ostream &, const vector3 &);
  inline friend std::istream &operator>>(std::istream &, vector3 &);
};

// Constructor
inline vector3::vector3(){
}

inline vector3::vector3(double xx, double yy, double zz){
  x = xx;
  y = yy;
  z = zz;
}

// Destructor
inline vector3::~vector3(){
}

// Die Zuweisung
inline vector3 &vector3::operator=(double zero){
#ifdef DEBUG
  if(zero != 0){
    cerr << "ERROR from vector3: Cannot assign a number to a vector.\n";
    myexit(1);
  }
#endif
  x = zero;
  y = zero;
  z = zero;
  return *this;
}
  
  inline vector3 &vector3::operator=(const vector3 &a){
    x = a.x;
    y = a.y;
    z = a.z;
    return *this;
  }
    
// Unaere Operatoren
inline vector3 operator+(const vector3 &a)
{
  return a;
}

inline vector3 operator-(const vector3 &a)
{
  return vector3(-a.x, -a.y, -a.z);
}

// Binaere Operatoren
inline double operator*(const vector3 &a, const vector3 &b)
{
  return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline vector3 operator+(const vector3 &a, const vector3 &b)
{
  return vector3(a.x + b.x, a.y + b.y, a.z + b.z);
}

inline vector3 operator-(const vector3 &a, const vector3 &b)
{
  return vector3(a.x - b.x, a.y - b.y, a.z - b.z);
}

inline vector3 operator*(const vector3 &a, double b)
{
  return vector3(a.x * b, a.y * b, a.z * b);
}

inline vector3 operator/(const vector3 &a, double b)
{
  return vector3(a.x / b, a.y / b, a.z / b);
}

inline vector3 operator*(double a, const vector3 &b)
{
  return vector3(a * b.x, a * b.y, a * b.z);
}

// Functions
// Modulus
inline double norm(const vector3 &a)
{
  return sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
}

// Polar angle of a vector
inline double polar3(vector3 &m){

  double theta;

  if(fabs(m.x) < EMACH && fabs(m.y) < EMACH){
    if(m.z >= 0){
      theta = 0;
    }
    else{
      theta = M_PI;
    }
  }
  else{
    theta = acos(m.z / norm(m));
  }
  
  return theta;
}

// Azimuth of a vector
inline double azimuth3(vector3 &m){

  double phi;

  if(fabs(m.x) < EMACH && fabs(m.y) < EMACH){
    phi = 0;
  }
  else{
    phi = atan2(m.y, m.x);
  }
  
  return phi;
}

// Cross product
inline vector3 cross(const vector3 &a, const vector3 &b){

  return vector3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
}


// Input, output
inline std::ostream &operator<<(std::ostream &os, const vector3 &a){
  int w = os.width(0);
  int p = os.precision(0);
#if defined(GCC3) || defined(GCC4)
  long f = os.flags(std::_Ios_Fmtflags(0));
#else
  long f = os.flags(0);
#endif
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << a.x;
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << a.y;
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << a.z;
  return os;
}

inline std::istream &operator>>(std::istream &is, vector3 &a){
  is >> a.x;
  is >> a.y;
  is >> a.z;
  return is;
}



#endif
