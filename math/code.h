
//
// ODE solver (complex)
//
#ifndef _CODE_
#define _CODE_

#include "dmatrix.h"
#include "cmatrix.h"

// The general driver function
void code(int odetype, int nvar, int kmax, int &kount, double x1, double x2, double dxsav, double eps, double h1,
	  complexdiagmatrix &ystart, doublediagmatrix &xp, complexmatrix &yp,
	  void (*odefunc)(double, complexdiagmatrix &, complexdiagmatrix &));


#endif
