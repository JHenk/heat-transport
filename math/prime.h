//
// Two primitive functions for primes
//

#ifndef _PRIME_
#define _PRIME_

// Test n on prime
// Returns 0 if non-prime, 1 otherwise
int primet(int n);


// Compute the n-th prime number
int prime(int n);

#endif
