//
// Functions for matrix inversion using LU decomposition and iterative
// improvement
//
// Adapted from Numerical Recipes
//
// JH 2/8/99

#ifndef _DO_LU_
#define _DO_LU_

#include "complex.h"
#include "cmatrix.h"

complextempdiagmatrix solvelu(const complexmatrix &a, const complexdiagmatrix &b);
complextempmatrix     solve2lu(const complexmatrix &A, const complexmatrix &B);
complextempmatrix     invlu(const complexmatrix &a);
complex               detlu(const complexmatrix &a);
complex               logdetlu(const complexmatrix &a);


#endif
