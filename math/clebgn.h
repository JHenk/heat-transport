// clebgn.h
//
// Prototypes for functions in clebgn.C, global variables
//
// Author: 1999 Dipl. Phys. Thomas Scheunemann
//        Gerhard Mercator Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//
//
// omni uses three sortations:
// kappa sortation: -1,1,-2,2,-3,...
// kappa-mu sortation: (-1,-1/2),(-1,+1/2),(1,-1/2),(1,+1/2),(-2,-3/2),(-2,-1/2),...
// l-m sortation: (0,0),(1,-1),(1,0),(1,1),(2,-2),(2,-1),(2,0),...
//
// All arrays contain values including lmax + 1.

#ifndef _CLEBGN_
#define _CLEBGN_

// lmax is the maximum angular momentum. All following variables depend on it.
extern int		lmax;

// kpmax is the number of points in the kappa sortation
extern int		kpmax;

// lmmax is the number of points in the l-m sortation
extern int		lmmax;

// kmmax is the number of points in the kappa-mu sortation
extern int		kmmax;


// The following arrays transform between the sortations.

// kmkp contains for a kappa-mu index the kappa index.
extern int		*kmkp;

// kml contains for a kappa-mu index the l of the chi_kappa_mu
extern int		*kml;

// kmm1 contains for a kappa-mu index the m of the spin-up component of the chi_kappa_mu
extern int		*kmm1;

// kmm2 contains for a kappa-mu index the m of the spin-down component of the chi_kappa_mu
extern int		*kmm2;

// kmlm1 contains for a kappa-mu index the l-m index of the spin-up component of the chi_kappa_mu
extern int		*kmlm1;

// kmlm2 contains for a kappa-mu index the l-m index of the spin-down component of the chi_kappa_mu
extern int		*kmlm2;

// cleb1 contains for a kappa-mu index the Clebsch Gordan coeffizient of the spin-up component of 
// chi_kappa_mu
extern double		*cleb1;

// cleb2 contains for a kappa-mu index the Clebsch Gordan coeffizient of the spin-down component of 
// chi_kappa_mu
extern double		*cleb2;

// trkm contains for a kappa-mu index the index of the time-reversed chi_kappa_mu
extern int		*trkm;

// trsg contains for a kappa-mu index a pre-factor of the time-reversed chi_kappa-mu
extern int		*trsg;

// kpk contains for a kappa index the kappa value
extern int		*kpk;

// kmk contains for a kappa-mu index the kappa value
extern int		*kmk;

// kmmu2 contains for a kappa-mu index the value of 2 * mu
extern int		*kmmu2;

// kmo contains for a kappa-mu index the index of the (-kappa-1, -mu)
extern int		*kmo;

// kmn contains for a kappa-mu index the index of the (-kappa, mu)
extern int		*kmn;

// fac contains a table of factorials
extern double		*fac;

void clebgn();
void print_clebgn(int mpiRank=0);

#endif
