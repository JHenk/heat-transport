//
// Distributions
//

#ifndef _DISTRIBUTION_
#define _DISTRIBUTION_

#include "complex.h"

double gaussian(double x, double mue, double sigma);
double cauchy(double x, double mue, double gamma);
double vonMisesFisher3(double kappa, double theta_0, double phi_0, double theta, double phi);
complex FermiDirac(complex e, double mue, double kt);
complex BoseEinstein(complex e, double mue, double kt);


#endif
