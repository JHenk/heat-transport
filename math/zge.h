#include "cmatrix.h"
#include "imatrix.h"

void zge(complexmatrix &A,  intdiagmatrix &INT, int N);
void zger(complexmatrix &A, intdiagmatrix &INT, int N);
void zsu(complexmatrix &A,  intdiagmatrix &INT, complexmatrix &B, int N);
void zsur(complexmatrix &A, intdiagmatrix &INT, complexmatrix &B, int N);




