// function.C
//
// Double and complex function class
//

#include <cstdlib>
#include <cstring>

#include "platform.h"

#include "complex.h"
#include "function.h"

using namespace std;

//
// Unaere Operatoren
//

doubletempfunction operator+(const doublefunction &a)

{
  doubletempfunction s(a.n);
  double *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++;
  }
  return s;
}

doubletempfunction operator-(const doublefunction &a)

{
  doubletempfunction s(a.n);
  double *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = -(*pa++);
  }
  return s;
}


//
// Binaere Operatoren
//

doubletempfunction operator*(const doublefunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempfunction s(a.n);
  double *ps = s.v;
  double *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * *pb++;
  }
  return s;
}

doubletempfunction operator/(const doublefunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempfunction s(a.n);
  double *ps = s.v;
  double *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ / *pb++;
  }
  return s;
}

doubletempfunction operator+(const doublefunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempfunction s(a.n);
  double *ps = s.v;
  double *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + *pb++;
  }
  return s;
}

doubletempfunction operator-(const doublefunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempfunction s(a.n);
  double *ps = s.v;
  double *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - *pb++;
  }
  return s;
}

doubletempfunction operator*(double a, const doublefunction &b)

{
  doubletempfunction s(b.n);
  double *ps = s.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a * *pb++;
  }
  return s;
}

doubletempfunction operator/(double a, const doublefunction &b)

{
  doubletempfunction s(b.n);
  double *ps = s.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a / *pb++;
  }
  return s;
}

doubletempfunction operator+(double a, const doublefunction &b)

{
  doubletempfunction s(b.n);
  double *ps = s.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a + *pb++;
  }
  return s;
}

doubletempfunction operator-(double a, const doublefunction &b)

{
  doubletempfunction s(b.n);
  double *ps = s.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a - *pb++;
  }
  return s;
}

doubletempfunction operator*(const doublefunction &a, double b)

{
  doubletempfunction s(a.n);
  double *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * b;
  }
  return s;
}

doubletempfunction operator/(const doublefunction &a, double b)

{
  doubletempfunction s(a.n);
  double *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ / b;
  }
  return s;
}

doubletempfunction operator+(const doublefunction &a, double b)

{
  doubletempfunction s(a.n);
  double *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + b;
  }
  return s;
}

doubletempfunction operator-(const doublefunction &a, double b)

{
  doubletempfunction s(a.n);
  double *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - b;
  }
  return s;
}

//
// Unaere Operatoren
//

complextempfunction operator+(const complexfunction &a)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++;
  }
  return s;
}

complextempfunction operator-(const complexfunction &a)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = -(*pa++);
  }
  return s;
}


//
// Binaere Operatoren
//

complextempfunction operator*(const complexfunction &a, const complexfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
#ifndef ORIGINAL
    double ar = pa->re;
    double ai = pa->im;
    double br = pb->re;
    double bi = pb->im;
    ps->re = ar * br - ai * bi;
    ps->im = ar * bi + ai * br;
    pa += 1;
    pb += 1;
    ps += 1;
#else
    *ps++ = *pa++ * *pb++;
#endif /* ORIGINAL */
  }
  return s;
}

complextempfunction operator/(const complexfunction &a, const complexfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ / *pb++;
  }
  return s;
}

complextempfunction operator+(const complexfunction &a, const complexfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
#ifndef ORIGINAL
    ps->re = pa->re + pb->re;
    ps->im = pa->im + pb->im;
    pa += 1;
    pb += 1;
    ps += 1;
#else
    *ps++ = *pa++ + *pb++;
#endif /* ORIGINAL */
  }
  return s;
}

complextempfunction operator-(const complexfunction &a, const complexfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
#ifndef ORIGINAL
    ps->re = pa->re - pb->re;
    ps->im = pa->im - pb->im;
    pa += 1;
    pb += 1;
    ps += 1;
#else
    *ps++ = *pa++ - *pb++;
#endif /* ORIGINAL */
  }
  return s;
}

complextempfunction operator*(double a, const complexfunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
#ifndef ORIGINAL
    ps->re = a * pb->re;
    ps->im = a * pb->im;
    pb += 1;
    ps += 1;
#else
    *ps++ = a * *pb++;
#endif /* ORIGINAL */
  }
  return s;
}

complextempfunction operator/(double a, const complexfunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a / *pb++;
  }
  return s;
}

complextempfunction operator+(double a, const complexfunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a + *pb++;
  }
  return s;
}

complextempfunction operator-(double a, const complexfunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a - *pb++;
  }
  return s;
}

complextempfunction operator*(const complexfunction &a, double b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
#ifndef ORIGINAL
    ps->re = pa->re * b;
    ps->im = pa->im * b;
    pa += 1;
    ps += 1;
#else
    *ps++ = *pa++ * b;
#endif /* ORIGINAL */
  }
  return s;
}

complextempfunction operator/(const complexfunction &a, double b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ / b;
  }
  return s;
}

complextempfunction operator+(const complexfunction &a, double b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + b;
  }
  return s;
}

complextempfunction operator-(const complexfunction &a, double b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - b;
  }
  return s;
}

complextempfunction operator*(const complex &a, const complexfunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  complex *pb = b.v;
#ifndef ORIGINAL
  double ar = a.re;
  double ai = a.im;
#endif /* ORIGINAL */
  int i = s.n;
  while(--i >= 0){
#ifndef ORIGINAL
    double br = pb->re;
    double bi = pb->im;
    ps->re = ar * br - ai * bi;
    ps->im = ar * bi + ai * br;
    pb += 1;
    ps += 1;
#else
    *ps++ = a * *pb++;
#endif /* ORIGINAL */
  }
  return s;
}

complextempfunction operator/(const complex &a, const complexfunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a / *pb++;
  }
  return s;
}

complextempfunction operator+(const complex &a, const complexfunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a + *pb++;
  }
  return s;
}

complextempfunction operator-(const complex &a, const complexfunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a - *pb++;
  }
  return s;
}

complextempfunction operator*(const complexfunction &a, const complex &b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
#ifndef ORIGINAL
  double br = b.re;
  double bi = b.im;
#endif /* ORIGINAL */
  int i = s.n;
  while(--i >= 0){
#ifndef ORIGINAL
    double ar = pa->re;
    double ai = pa->im;
    ps->re = ar * br - ai * bi;
    ps->im = ar * bi + ai * br;
    pa += 1;
    ps += 1;
#else
    *ps++ = *pa++ * b;
#endif /* ORIGINAL */
  }
  return s;
}

complextempfunction operator/(const complexfunction &a, const complex &b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ / b;
  }
  return s;
}

complextempfunction operator+(const complexfunction &a, const complex &b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + b;
  }
  return s;
}

complextempfunction operator-(const complexfunction &a, const complex &b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - b;
  }
  return s;
}

complextempfunction operator*(const complexfunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
#ifndef ORIGINAL
    double ar = pa->re;
    double ai = pa->im;
    double bb = *pb;
    ps->re = ar * bb;
    ps->im = ai * bb;
    pa += 1;
    pb += 1;
    ps += 1;
#else
    *ps++ = *pa++ * *pb++;
#endif /* ORIGINAL */
  }
  return s;
}

complextempfunction operator/(const complexfunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ / *pb++;
  }
  return s;
}

complextempfunction operator+(const complexfunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + *pb++;
  }
  return s;
}

complextempfunction operator-(const complexfunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - *pb++;
  }
  return s;
}

complextempfunction operator*(const doublefunction &a, const complexfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  double *pa = a.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
#ifndef ORIGINAL
    double aa = *pa;
    double br = pb->re;
    double bi = pb->im;
    ps->re = aa * br;
    ps->im = aa * bi;
    pa += 1;
    pb += 1;
    ps += 1;
#else
    *ps++ = *pa++ * *pb++;
#endif /* ORIGINAL */
  }
  return s;
}

complextempfunction operator/(const doublefunction &a, const complexfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  double *pa = a.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ / *pb++;
  }
  return s;
}

complextempfunction operator+(const doublefunction &a, const complexfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  double *pa = a.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + *pb++;
  }
  return s;
}

complextempfunction operator-(const doublefunction &a, const complexfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from complextempfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
  complex *ps = s.v;
  double *pa = a.v;
  complex *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - *pb++;
  }
  return s;
}

complextempfunction operator*(const complex &a, const doublefunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  double *pb = b.v;
#ifndef ORIGINAL
  double ar = a.re;
  double ai = a.im;
#endif /* ORIGINAL */
  int i = s.n;
  while(--i >= 0){
#ifndef ORIGINAL
    double bb = *pb;
    ps->re = ar * bb;
    ps->im = ai * bb;
    pb += 1;
    ps += 1;
#else
    *ps++ = a * *pb++;
#endif /* ORIGINAL */
  }
  return s;
}

complextempfunction operator/(const complex &a, const doublefunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a / *pb++;
  }
  return s;
}

complextempfunction operator+(const complex &a, const doublefunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a + *pb++;
  }
  return s;
}

complextempfunction operator-(const complex &a, const doublefunction &b)

{
  complextempfunction s(b.n);
  complex *ps = s.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a - *pb++;
  }
  return s;
}

complextempfunction operator*(const doublefunction &a, const complex &b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  double *pa = a.v;
#ifndef ORIGINAL
  double br = b.re;
  double bi = b.im;
#endif /* ORIGINAL */
  int i = s.n;
  while(--i >= 0){
#ifndef ORIGINAL
    double aa = *pa;
    ps->re = aa * br;
    ps->im = aa * bi;
    pa += 1;
    ps += 1;
#else
    *ps++ = *pa++ * b;
#endif /* ORIGINAL */
  }
  return s;
}

complextempfunction operator/(const doublefunction &a, const complex &b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ / b;
  }
  return s;
}

complextempfunction operator+(const doublefunction &a, const complex &b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + b;
  }
  return s;
}

complextempfunction operator-(const doublefunction &a, const complex &b)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - b;
  }
  return s;
}

//
// Funktionen
//

complextempfunction conj(const complexfunction &a)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = conj(*pa++);
  }
  return s;
}

complextempfunction real(const complexfunction &a)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = real(*pa++);
  }
  return s;
}

complextempfunction imag(const complexfunction &a)

{
  complextempfunction s(a.n);
  complex *ps = s.v;
  complex *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = imag(*pa++);
  }
  return s;
}

//
// Integrationen
//

doubletempfunction integral(const doublefunction &a, double b)

{
#ifdef DEBUG
  if(a.n <= 0){
    cerr << "ERROR from doubletempfunction integral(): Invalid function\n";
    myexit(1);
  }
#endif
  doubletempfunction s(a.n);
  double sum = b;
  int i3 = (s.n - 1) / 3;
  int n3 = s.n - 3 * i3;
  double *ps = s.v;
  double *pa = a.v;
  double f0 = *pa++;
  double f1 = f0;
  double f2 = f0;
  double f3 = f0;
  *ps++ = sum;
  while(--i3 >= 0){
    f0 = f3;
    f1 = *pa++;
    f2 = *pa++;
    f3 = *pa++;
    sum += (9 * f0 + 19 * f1 - 5 * f2 + f3) / 24;
    *ps++ = sum;
    sum += (-f0 + 13 * f1 + 13 * f2 - f3) / 24;
    *ps++ = sum;
    sum += (f0 - 5 * f1 + 19 * f2 + 9 * f3) / 24;
    *ps++ = sum;
  }
  switch(n3){
  case 1:
    break;
  case 2:
    f0 = f1;
    f1 = f2;
    f2 = f3;
    f3 = *pa++;
    sum += (f0 - 5 * f1 + 19 * f2 + 9 * f3) / 24;
    *ps++ = sum;
    break;
  case 3:
    f0 = f2;
    f1 = f3;
    f2 = *pa++;
    f3 = *pa++;
    sum += (-f0 + 13 * f1 + 13 * f2 - f3) / 24;
    *ps++ = sum;
    sum += (f0 - 5 * f1 + 19 * f2 + 9 * f3) / 24;
    *ps++ = sum;
    break;
  }
  return s;
}

doubletempfunction exp(const doublefunction &a)
{
#ifdef DEBUG
  if(a.n <= 0){
    cerr << "ERROR from doubletempfunction exp(): Invalid function\n";
    myexit(1);
  }
#endif
  doubletempfunction s(a.n);
  double *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = exp(*pa++);
  }
  return s;
}

double integral(const doublefunction &a)
{
#ifdef DEBUG
  if(a.n <= 0){
    cerr << "ERROR from double integral(doublefunction): Invalid function\n";
    myexit(1);
  }
#endif
  double sum = 0;
  int i3 = (a.n - 1) / 3;
  int n3 = a.n - 3 * i3;
  double *pa = a.v;
  double f0 = *pa++;
  double f1 = f0;
  double f2 = f0;
  double f3 = f0;
  while(--i3 >= 0){
    f0 = f3;
    f1 = *pa++;
    f2 = *pa++;
    f3 = *pa++;
    sum += 3 * (f0 + 3 * f1 + 3 * f2 + f3) / 8;
  }
  switch(n3){
  case 1:
    break;
  case 2:
    f0 = f1;
    f1 = f2;
    f2 = f3;
    f3 = *pa++;
    sum += (f0 - 5 * f1 + 19 * f2 + 9 * f3) / 24;
    break;
  case 3:
    f0 = f2;
    f1 = f3;
    f2 = *pa++;
    f3 = *pa++;
    sum += (f1 + 4 * f2 + f3) / 3;
    break;
  }
  return sum;
}

complextempfunction integral(const complexfunction &a, const complex &b)

{
#ifdef DEBUG
  if(a.n <= 0){
    cerr << "ERROR from complextempfunction integral(): Invalid function\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
#ifndef ORIGINAL
  double sumr = b.re;
  double sumi = b.im;
#else
  complex sum = b;
#endif /* ORIGINAL */
  int i3 = (s.n - 1) / 3;
  int n3 = s.n - 3 * i3;
  complex *ps = s.v;
  complex *pa = a.v;
#ifndef ORIGINAL
  double f0r = pa->re;
  double f0i = pa->im;
  pa += 1;
  double f1r = f0r;
  double f1i = f0i;
  double f2r = f0r;
  double f2i = f0i;
  double f3r = f0r;
  double f3i = f0i;
  ps->re = sumr;
  ps->im = sumi;
  ps += 1;
#else
  complex f0 = *pa++;
  complex f1 = f0;
  complex f2 = f0;
  complex f3 = f0;
  *ps++ = sum;
#endif /* ORIGINAL */
  while(--i3 >= 0){
#ifndef ORIGINAL
    f0r = f3r;
    f0i = f3i;
    f1r = pa->re;
    f1i = pa->im;
    pa += 1;
    f2r = pa->re;
    f2i = pa->im;
    pa += 1;
    f3r = pa->re;
    f3i = pa->im;
    pa += 1;
    sumr += (9 * f0r + 19 * f1r - 5 * f2r + f3r) / 24;
    sumi += (9 * f0i + 19 * f1i - 5 * f2i + f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
    sumr += (-f0r + 13 * f1r + 13 * f2r - f3r) / 24;
    sumi += (-f0i + 13 * f1i + 13 * f2i - f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
    sumr += (f0r - 5 * f1r + 19 * f2r + 9 * f3r) / 24;
    sumi += (f0i - 5 * f1i + 19 * f2i + 9 * f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
#else
    f0 = f3;
    f1 = *pa++;
    f2 = *pa++;
    f3 = *pa++;
    sum += (9 * f0 + 19 * f1 - 5 * f2 + f3) / 24;
    *ps++ = sum;
    sum += (-f0 + 13 * f1 + 13 * f2 - f3) / 24;
    *ps++ = sum;
    sum += (f0 - 5 * f1 + 19 * f2 + 9 * f3) / 24;
    *ps++ = sum;
#endif /* ORIGINAL */
  }
  switch(n3){
  case 1:
    break;
  case 2:
#ifndef ORIGINAL
    f0r = f1r;
    f0i = f1i;
    f1r = f2r;
    f1i = f2i;
    f2r = f3r;
    f2i = f3i;
    f3r = pa->re;
    f3i = pa->im;
    pa += 1;
    sumr += (f0r - 5 * f1r + 19 * f2r + 9 * f3r) / 24;
    sumi += (f0i - 5 * f1i + 19 * f2i + 9 * f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
#else
    f0 = f1;
    f1 = f2;
    f2 = f3;
    f3 = *pa++;
    sum += (f0 - 5 * f1 + 19 * f2 + 9 * f3) / 24;
    *ps++ = sum;
#endif /* ORIGINAL */
    break;
  case 3:
#ifndef ORIGINAL
    f0r = f2r;
    f0i = f2i;
    f1r = f3r;
    f1i = f3i;
    f2r = pa->re;
    f2i = pa->im;
    pa += 1;
    f3r = pa->re;
    f3i = pa->im;
    pa += 1;
    sumr += (-f0r + 13 * f1r + 13 * f2r - f3r) / 24;
    sumi += (-f0i + 13 * f1i + 13 * f2i - f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
    sumr += (f0r - 5 * f1r + 19 * f2r + 9 * f3r) / 24;
    sumi += (f0i - 5 * f1i + 19 * f2i + 9 * f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
#else
    f0 = f2;
    f1 = f3;
    f2 = *pa++;
    f3 = *pa++;
    sum += (-f0 + 13 * f1 + 13 * f2 - f3) / 24;
    *ps++ = sum;
    sum += (f0 - 5 * f1 + 19 * f2 + 9 * f3) / 24;
    *ps++ = sum;
#endif /* ORIGINAL */
    break;
  }
  return s;
}

complextempfunction integral(const complexfunction &a, double b)

{
#ifdef DEBUG
  if(a.n <= 0){
    cerr << "ERROR from complextempfunction integral(): Invalid function\n";
    myexit(1);
  }
#endif
  complextempfunction s(a.n);
#ifndef ORIGINAL
  double sumr = b;
  double sumi = 0;
#else
  complex sum = b;
#endif /* ORIGINAL */
  int i3 = (s.n - 1) / 3;
  int n3 = s.n - 3 * i3;
  complex *ps = s.v;
  complex *pa = a.v;
#ifndef ORIGINAL
  double f0r = pa->re;
  double f0i = pa->im;
  pa += 1;
  double f1r = f0r;
  double f1i = f0i;
  double f2r = f0r;
  double f2i = f0i;
  double f3r = f0r;
  double f3i = f0i;
  ps->re = sumr;
  ps->im = sumi;
  ps += 1;
#else
  complex f0 = *pa++;
  complex f1 = f0;
  complex f2 = f0;
  complex f3 = f0;
  *ps++ = sum;
#endif /* ORIGINAL */
  while(--i3 >= 0){
#ifndef ORIGINAL
    f0r = f3r;
    f0i = f3i;
    f1r = pa->re;
    f1i = pa->im;
    pa += 1;
    f2r = pa->re;
    f2i = pa->im;
    pa += 1;
    f3r = pa->re;
    f3i = pa->im;
    pa += 1;
    sumr += (9 * f0r + 19 * f1r - 5 * f2r + f3r) / 24;
    sumi += (9 * f0i + 19 * f1i - 5 * f2i + f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
    sumr += (-f0r + 13 * f1r + 13 * f2r - f3r) / 24;
    sumi += (-f0i + 13 * f1i + 13 * f2i - f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
    sumr += (f0r - 5 * f1r + 19 * f2r + 9 * f3r) / 24;
    sumi += (f0i - 5 * f1i + 19 * f2i + 9 * f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
#else
    f0 = f3;
    f1 = *pa++;
    f2 = *pa++;
    f3 = *pa++;
    sum += (9 * f0 + 19 * f1 - 5 * f2 + f3) / 24;
    *ps++ = sum;
    sum += (-f0 + 13 * f1 + 13 * f2 - f3) / 24;
    *ps++ = sum;
    sum += (f0 - 5 * f1 + 19 * f2 + 9 * f3) / 24;
    *ps++ = sum;
#endif /* ORIGINAL */
  }
  switch(n3){
  case 1:
    break;
  case 2:
#ifndef ORIGINAL
    f0r = f1r;
    f0i = f1i;
    f1r = f2r;
    f1i = f2i;
    f2r = f3r;
    f2i = f3i;
    f3r = pa->re;
    f3i = pa->im;
    pa += 1;
    sumr += (f0r - 5 * f1r + 19 * f2r + 9 * f3r) / 24;
    sumi += (f0i - 5 * f1i + 19 * f2i + 9 * f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
#else
    f0 = f1;
    f1 = f2;
    f2 = f3;
    f3 = *pa++;
    sum += (f0 - 5 * f1 + 19 * f2 + 9 * f3) / 24;
    *ps++ = sum;
#endif /* ORIGINAL */
    break;
  case 3:
#ifndef ORIGINAL
    f0r = f2r;
    f0i = f2i;
    f1r = f3r;
    f1i = f3i;
    f2r = pa->re;
    f2i = pa->im;
    pa += 1;
    f3r = pa->re;
    f3i = pa->im;
    pa += 1;
    sumr += (-f0r + 13 * f1r + 13 * f2r - f3r) / 24;
    sumi += (-f0i + 13 * f1i + 13 * f2i - f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
    sumr += (f0r - 5 * f1r + 19 * f2r + 9 * f3r) / 24;
    sumi += (f0i - 5 * f1i + 19 * f2i + 9 * f3i) / 24;
    ps->re = sumr;
    ps->im = sumi;
    ps += 1;
#else
    f0 = f2;
    f1 = f3;
    f2 = *pa++;
    f3 = *pa++;
    sum += (-f0 + 13 * f1 + 13 * f2 - f3) / 24;
    *ps++ = sum;
    sum += (f0 - 5 * f1 + 19 * f2 + 9 * f3) / 24;
    *ps++ = sum;
#endif /* ORIGINAL */
    break;
  }
  return s;
}

complex integral(const complexfunction &a)

{
#ifdef DEBUG
  if(a.n <= 0){
    cerr << "ERROR from complex integral(complexfunction): Invalid function\n";
    myexit(1);
  }
#endif
#ifndef ORIGINAL
  double sumr = 0;
  double sumi = 0;
#else
  complex sum = 0;
#endif /* ORIGINAL */
  int i3 = (a.n - 1) / 3;
  int n3 = a.n - 3 * i3;
  complex *pa = a.v;
#ifndef ORIGINAL
  double f0r = pa->re;
  double f0i = pa->im;
  pa += 1;
  double f1r = f0r;
  double f1i = f0i;
  double f2r = f0r;
  double f2i = f0i;
  double f3r = f0r;
  double f3i = f0i;
#else
  complex f0 = *pa++;
  complex f1 = f0;
  complex f2 = f0;
  complex f3 = f0;
#endif /* ORIGINAL */
  while(--i3 >= 0){
#ifndef ORIGINAL
    f0r = f3r;
    f0i = f3i;
    f1r = pa->re;
    f1i = pa->im;
    pa += 1;
    f2r = pa->re;
    f2i = pa->im;
    pa += 1;
    f3r = pa->re;
    f3i = pa->im;
    pa += 1;
    sumr += 3 * (f0r + 3 * f1r + 3 * f2r + f3r) / 8;
    sumi += 3 * (f0i + 3 * f1i + 3 * f2i + f3i) / 8;
#else
    f0 = f3;
    f1 = *pa++;
    f2 = *pa++;
    f3 = *pa++;
    sum += 3 * (f0 + 3 * f1 + 3 * f2 + f3) / 8;
#endif /* ORIGINAL */
  }
  switch(n3){
  case 1:
    break;
  case 2:
#ifndef ORIGINAL
    f0r = f1r;
    f0i = f1i;
    f1r = f2r;
    f1i = f2i;
    f2r = f3r;
    f2i = f3i;
    f3r = pa->re;
    f3i = pa->im;
    pa += 1;
    sumr += (f0r - 5 * f1r + 19 * f2r + 9 * f3r) / 24;
    sumi += (f0i - 5 * f1i + 19 * f2i + 9 * f3i) / 24;
#else
    f0 = f1;
    f1 = f2;
    f2 = f3;
    f3 = *pa++;
    sum += (f0 - 5 * f1 + 19 * f2 + 9 * f3) / 24;
#endif /* ORIGINAL */
    break;
  case 3:
#ifndef ORIGINAL
    f0r = f2r;
    f0i = f2i;
    f1r = f3r;
    f1i = f3i;
    f2r = pa->re;
    f2i = pa->im;
    pa += 1;
    f3r = pa->re;
    f3i = pa->im;
    pa += 1;
    sumr += (f1r + 4 * f2r + f3r) / 3;
    sumi += (f1i + 4 * f2i + f3i) / 3;
#else
    f0 = f2;
    f1 = f3;
    f2 = *pa++;
    f3 = *pa++;
    sum += (f1 + 4 * f2 + f3) / 3;
#endif /* ORIGINAL */
    break;
  }
#ifndef ORIGINAL
  return cmplx(sumr, sumi);
#else
  return sum;
#endif /* ORIGINAL */
}

//
// Ein- und Ausgabe
//

ostream &operator<<(ostream &os, const doublefunction &a)

{
  int w = os.width(0);
  int p = os.precision(0);
#if defined(GCC3) || defined(GCC4)
  long f = os.flags(std::_Ios_Fmtflags(0));
#else
  long f = os.flags(0);
#endif
  double *pa = a.v;
  int i = a.n;
  while(--i >= 0){
    os.width(w);
    os.precision(p);
#if defined(GCC3) || defined(GCC4)
    os.flags(std::_Ios_Fmtflags(f));
#else
    os.flags(f);
#endif
    os << *pa++;
    if(i != 0){
      os << " ";
    }
  }
  os << "\n";
  return os;
}

istream &operator>>(istream &is, const doublefunction &a)

{
  double *pa = a.v;
  int i = a.n;
  while(--i >= 0){
    is >> *pa++;
  }
  return is;
}

ostream &operator<<(ostream &os, const complexfunction &a)

{
  int w = os.width(0);
  int p = os.precision(0);
#if defined(GCC3) || defined(GCC4)
  long f = os.flags(std::_Ios_Fmtflags(0));
#else
  long f = os.flags(0);
#endif
  complex *pa = a.v;
  int i = a.n;
  while(--i >= 0){
    os.width(w);
    os.precision(p);
#if defined(GCC3) || defined(GCC4)
    os.flags(std::_Ios_Fmtflags(f));
#else
    os.flags(f);
#endif
    os << *pa++;
    if(i != 0){
      os << " ";
    }
  }
  os << "\n";
  return os;
}

istream &operator>>(istream &is, const complexfunction &a)

{
  complex *pa = a.v;
  int i = a.n;
  while(--i >= 0){
    is >> *pa++;
  }
  return is;
}
