// do_eigen.h
//
// enthaelt den Prototyp der in do_eigen.C definierten Funktion
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _DO_EIGEN_
#define _DO_EIGEN_

#include "complex.h"
#include "cmatrix.h"

void do_eigen(complex *a, complex *e, complex *v, int n);


#endif
