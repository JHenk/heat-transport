
#ifndef _GAUSS_
#define _GAUSS_

#include "complex.h"

// Compute the abscissas and weights used in a n-point Gauss-Legendre
// quadrature
// Taken from Numerical Recipes
void gauleg(double x1, double x2, double *x, double *w, int n);

// Perform a N-point Gaussian quadrature (see Abramowitz/Steegun 25.4.30 and
// Table 25.4.)
//
// N       number of abscissas
// weight  weighting factors
// values  ordinates
//
// returns the integral from left to right
//
// Note: the abscissas and weights have to be transformed if the interval is
// NOT [-1, ..., + 1].
complex gauss(int N, complex *weight, complex *values);


// Read abscissas and weights for N-point Gaussian quadrature
// N          number of abscissas
// abscissas  guess what: the abscissas
// weight     weighting factors, really

void read_gauss(int N, double *abscissas, double *weight);


// Transform abscissas for N-point Gaussian quadrature
// N          number of abscissas
// abscissas  the abscissas in [-1, ..., +1], overwritten

void tform_gauss(int N, double *abscissas, double *weight, double left, double right);

// Transform abscissas and weights for N-point Gaussian quadrature
// for logarithmic mesh (see note)
//
// N          number of abscissas
// abscissas  the abscissas in [-1, ..., +1], overwritten
void tform2_gauss(int N, double *abscissas, double *weight, double left, double right, double beta);


// Compute the abscissas and weights used in a n-point Gauss-Laguerre
// quadrature
void gaulag(double *x, double *w, int n);

#endif
