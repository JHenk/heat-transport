//
// Functions for the computation of Gaunt coefficients
//
// JH - 26/05/99

#ifndef _GAUNT_
#define _GAUNT_

// Calculates of ylm* x yllmm* x yl'm' x sqrt(4pi)
// condon & shortley convention -- using racah equation quoted by rotenberg
double gaunt(int l1, int l2, int l3, int m1, int m2, int m3);

#endif
