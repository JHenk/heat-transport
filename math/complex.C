// complex.C
//
// Complex numbers
//
// Based on Complex.cc from libg++-2.7.0a
// Copyright (C) 1988 Free Software Foundation
//    written by Doug Lea (dl@rocky.oswego.edu)
//

#include <iostream>
#include <cmath>
#include <cstdlib>

#include "platform.h"

#include "complex.h"

using namespace std;

complex operator/(const complex& x, const complex& y){
  double den = fabs(y.re) + fabs(y.im);
  if(den == 0.0) {
    cerr << "ERROR from complex: Attempted division by zero.\n";
    myexit(1);
  }
  double xrden = x.re / den;
  double xiden = x.im / den;
  double yrden = y.re / den;
  double yiden = y.im / den;
  double nrm   = yrden * yrden + yiden * yiden;
  return cmplx((xrden * yrden + xiden * yiden) / nrm,
	       (xiden * yrden - xrden * yiden) / nrm);
}

complex& complex::operator/=(const complex& y){
  double den = fabs(y.re) + fabs(y.im);
  if(den == 0.0) {
    cerr << "ERROR from complex: Attempted division by zero.\n";
    myexit(1);
  }
  double xrden = re / den;
  double xiden = im / den;
  double yrden = y.re / den;
  double yiden = y.im / den;
  double nrm   = yrden * yrden + yiden * yiden;
  re = (xrden * yrden + xiden * yiden) / nrm;
  im = (xiden * yrden - xrden * yiden) / nrm;
  return *this;
}

complex operator/(double x, const complex& y){
  double den = norm(y);
  if (den == 0.0) {
    cerr << "ERROR from complex: Attempted division by zero.\n";
    myexit(1);
  }
  return cmplx((x * y.re) / den, -(x * y.im) / den);
}

complex operator/(const complex& x, double y){
  if (fabs(y) < EMACH) {
    cerr << "ERROR from complex: Attempted division by zero.\n";
    myexit(1);
  }
  return cmplx(x.re / y, x.im / y);
}


complex& complex::operator/=(double y){
  if (y == 0.0) {
    cerr << "ERROR from complex: Attempted division by zero.\n";
    myexit(1);
  }
  re /= y;  im /= y;
  return *this;
}


complex exp(const complex& x)

{
double r = exp(x.re);
return cmplx(r * cos(x.im), r * sin(x.im));
}

complex cosh(const complex& x)

{
return cmplx(cos(x.im) * cosh(x.re), 
               sin(x.im) * sinh(x.re));
}

complex sinh(const complex& x)

{
return cmplx(cos(x.im) * sinh(x.re), 
               sin(x.im) * cosh(x.re));
}

complex cos(const complex& x)

{
return cmplx(cos(x.re) * cosh(x.im), 
              -sin(x.re) * sinh(x.im));
}

complex sin(const complex& x)

{
return cmplx(sin(x.re) * cosh(x.im), 
               cos(x.re) * sinh(x.im));
}

complex acos(const complex& x)

{
double r = x.re;
double i = x.im;

// Vgl. Abramowitz & Stegun, p. 80f
double alpha = 0.5 * sqrt((r+1)*(r+1) + i*i)
             + 0.5 * sqrt((r-1)*(r-1) + i*i);
double beta  = 0.5 * sqrt((r+1)*(r+1) + i*i)
             - 0.5 * sqrt((r-1)*(r-1) + i*i);

// return the principal root (k = 0)
return cmplx(acos(beta), -log(alpha + sqrt(alpha * alpha - 1)));
}

complex asin(const complex& x)

{
double r = x.re;
double i = x.im;

// Vgl. Abramowitz & Stegun, p. 80f
double alpha = 0.5 * sqrt((r+1)*(r+1) + i*i)
             + 0.5 * sqrt((r-1)*(r-1) + i*i);
double beta  = 0.5 * sqrt((r+1)*(r+1) + i*i)
             - 0.5 * sqrt((r-1)*(r-1) + i*i);

// return the principal root (k = 0)
return cmplx(asin(beta), log(alpha + sqrt(alpha * alpha - 1)));
}

complex log(const complex& x)

{
double h = hypot(x.real(), x.imag());
if (h <= 0.0){
	cerr << "ERROR from complex log(): Attempted log of zero magnitude number.\n";
	myexit(1);
	}
return cmplx(log(h), atan2(x.im, x.re));
}

complex pow(const complex& x, const complex& p){
  double h = hypot(x.real(), x.imag());
  if (h <= 0.0){
    cerr << "ERROR from complex pow(): Attempted power of zero magnitude number.\n";
    myexit(1);
  }
  double a = atan2(x.im, x.re);
  double lr = pow(h, p.re);
  double li = p.re * a;
  if (fabs(p.im) > 0.0){
    lr /= exp(p.im * a);
    li += p.im * log(h);
  }
  return cmplx(lr * cos(li), lr * sin(li));
}

complex pow(const complex& x, double p){
  double h = hypot(x.real(), x.imag());
  if (h <= 0.0){
    cerr << "ERROR from complex pow(): Attempted power of zero magnitude number.\n";
    myexit(1);
  }
  double lr = pow(h, p);
  double a = atan2(x.im, x.re);
  double li = p * a;
  return cmplx(lr * cos(li), lr * sin(li));
}


complex sqrt(const complex& x){
  if(fabs(x.re) < EMACH && fabs(x.im) < EMACH){
    return cmplx(0.0, 0.0);
  }
  else{
    double s = sqrt((fabs(x.re) + hypot(x.re, x.im)) * 0.5);
    double d = (x.imag() / s) * 0.5;
    if (x.re > 0.0){
      return cmplx(s, d);
    }
    else if (x.im >= 0.0){
      return cmplx(d, s);
    }
    else{
      return cmplx(-d, -s);
    }
  }
}

complex pow(const complex& x, int p)

{
if (p == 0){
	return cmplx(1.0, 0.0);
	}
else if (x == 0.0){
	return cmplx(0.0, 0.0);
	}
else
	{
	complex res = cmplx(1.0, 0.0);
	complex b = x;
	if (p < 0)
		{
		p = -p;
		b = 1.0 / b;
		}
	for(;;)
		{
		if (p & 1)
			res *= b;
		if ((p >>= 1) == 0)
			return res;
		else
			b *= b;
		}
	//	return cmplx(0.0, 0.0);
	}
}

ostream& operator<<(ostream& os, const complex& x)

{
int w = os.width(0);
int p = os.precision(0);
#if defined(GCC3) || defined(GCC4)
 long f = os.flags(std::_Ios_Fmtflags(0));
#else
 long f = os.flags(0);
#endif
os << "(";
os.width(w);
os.precision(p);
#if defined(GCC3) || defined(GCC4)
 os.flags(std::_Ios_Fmtflags(f));
#else
 os.flags(f);
#endif
os << x.re;
os << ",";
os.width(w);
os.precision(p);
#if defined(GCC3) || defined(GCC4)
 os.flags(std::_Ios_Fmtflags(f));
#else
 os.flags(f);
#endif
os << x.im;
os << ")";
return os;
}

istream& operator>>(istream& is, complex& x)

{
double r, i;
char ch;

is >> ws;
is.get(ch);
if (ch == '(') {
	is >> r;
	is >> ws;
	is.get(ch);
	if (ch == ',') {
		is >> i;
		is >> ws;
		is.get(ch);
		}
	else{
		i = 0;
		}
	if (ch != ')'){
		is.clear(ios::failbit);
		}
	}
else{
	is.putback(ch);
	is >> r;
	i = 0;
	}
x = cmplx(r, i);
return is;
}
