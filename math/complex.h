// complex.h
//
// deklariert komplexe Zahlen, sowie mathematische Operationen zwischen
// komplexen Zahlen.
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//
// Diese Datei basiert auf Complex.h aus libg++-2.7.0a
// Copyright (C) 1988 Free Software Foundation
//    written by Doug Lea (dl@rocky.oswego.edu)
//

#ifndef _COMPLEX_
#define _COMPLEX_

#include <iostream>
#include <cmath>

#include "constants.h"

class complex{
public:
  double re;
  double im;
public:
  double real() const;
  double imag() const;

#ifdef __GNUC__
  complex() {};
  complex(const complex& y) :re(y.re), im(y.im) {};
  complex(double r, double i=0) :re(r), im(i) {};
#endif

  complex& operator=(const complex& y);
  complex& operator=(double y);

  complex& operator+=(const complex& y);
  complex& operator+=(double y);
  complex& operator-=(const complex& y);
  complex& operator-=(double y);
  complex& operator*=(const complex& y);
  complex& operator*=(double y);
  complex& operator/=(const complex& y); 
  complex& operator/=(double y); 
};


// non-inline functions

complex operator/(const complex& x, const complex& y);
complex operator/(const complex& x, double y);
complex operator/(double   x, const complex& y);

complex cos(const complex& x);
complex sin(const complex& x);

complex acos(const complex& x);
complex asin(const complex& x);

complex cosh(const complex& x);
complex sinh(const complex& x);

complex exp(const complex& x);
complex log(const complex& x);

complex pow(const complex& x, int p);
complex pow(const complex& x, const complex& p);
complex pow(const complex& x, double y);
complex sqrt(const complex& x);
 
std::istream& operator>>(std::istream& s, complex& x);
std::ostream& operator<<(std::ostream& s, const complex& x);

// inline members

#ifdef __GNUC__
#define cmplx complex
#else

inline complex cmplx(double x, double y)

{
  class complex z;

  z.re = x;
  z.im = y;
  return z;
}

#endif

inline double complex::real() const

{
  return re;
}

inline double complex::imag() const

{
  return im;
}

inline complex&  complex::operator=(const complex& y) 

{ 
  re = y.re;
  im = y.im;
  return *this;
} 

inline complex&  complex::operator=(double y) 

{ 
  re = y;
  im = 0;
  return *this;
} 

inline complex& complex::operator+=(const complex& y)

{ 
  re += y.re;
  im += y.im;
  return *this;
}

inline complex& complex::operator+=(double y)

{ 
  re += y;
  return *this;
}

inline complex& complex::operator-=(const complex& y)

{ 
  re -= y.re;
  im -= y.im;
  return *this;
}

inline complex& complex::operator-=(double y)

{ 
  re -= y;
  return *this;
}

inline complex& complex::operator*=(const complex& y)

{  
  double r = re * y.re - im * y.im;
  im = re * y.im + im * y.re; 
  re = r; 
  return *this; 
}

inline complex& complex::operator*=(double y)

{  
  re *=  y;
  im *=  y;
  return *this; 
}

//  functions

inline int operator==(const complex& x, const complex& y)
{
  return ((fabs(x.re - y.re) < EMACH) && (fabs(x.im - y.im) < EMACH));
}

inline int operator==(const complex& x, double y)
{
  return ((fabs(x.im) < EMACH) && (fabs(x.re - y) < EMACH));
}

inline int operator!=(const complex& x, const complex& y)
{
  return (fabs(x.re - y.re) >= EMACH) || (fabs(x.im - y.im) >= EMACH);
}

inline int operator!=(const complex& x, double y)

{
  return (fabs(x.im) >= EMACH) || (fabs(x.re - y) >= EMACH);
}

inline complex operator+(const complex& x)

{
  return x;
}

inline complex operator-(const complex& x)

{
  return cmplx(-x.re, -x.im);
}

inline complex conj(const complex& x)

{
  return cmplx(x.re, -x.im);
}

inline complex operator+(const complex& x, const complex& y)

{
  return cmplx(x.re + y.re, x.im + y.im);
}

inline complex operator+(const complex& x, double y)

{
  return cmplx(x.re + y, x.im);
}

inline complex operator+(double x, const complex& y)

{
  return cmplx(x + y.re, y.im);
}

inline complex operator-(const complex& x, const complex& y)

{
  return cmplx(x.re - y.re, x.im - y.im);
}

inline complex operator-(const complex& x, double y)

{
  return cmplx(x.re - y, x.im);
}

inline complex operator-(double x, const complex& y)

{
  return cmplx(x - y.re, -y.im);
}

inline complex operator*(const complex& x, const complex& y)

{
  return cmplx(x.re * y.re - x.im * y.im, 
	       x.re * y.im + x.im * y.re);
}

inline complex operator*(const complex& x, double y)

{
  return cmplx(x.re * y, x.im * y);
}

inline complex operator*(double x, const complex& y)

{
  return cmplx(x * y.re, x * y.im);
}

inline double real(const complex& x)

{
  return x.re;
}

inline double imag(const complex& x)

{
  return x.im;
}

inline double abs(const complex& x)

{
  return hypot(x.re, x.im);
}

inline double norm(const complex& x)

{
  return (x.re * x.re + x.im * x.im);
}

inline double arg(const complex& x)

{
  return atan2(x.im, x.re);
}

inline complex polar(double r, double t)

{
  return cmplx(r * cos(t), r * sin(t));
}

#endif
