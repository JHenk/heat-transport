// smatrix.C
//
// Operation on double super matrices
//

#include <iostream>
#include <cmath>

#include "platform.h"

#include "dsmatrix.h"

using namespace std;

//
// Der Kopierkonstruktor
//

doublesupermatrix::doublesupermatrix(const doublesupermatrix &z)

{
// Abmessungen kopieren
r = z.r;
c = z.c;
// Groesse kopieren
n = z.n;
// Abmessungen je Block kopieren
rs = z.rs;
cs = z.cs;
// Speicher anfordern
m = new doublematrix[n];
int i = n;
doublematrix *pm = m;
doublematrix *pz = z.m;
while( --i >= 0){
	if(used(*pz)){
		*pm = *pz;
		}
	pz += 1;
	pm += 1;
	}
}

//
// Die Zuweisung
//

doublesupermatrix &doublesupermatrix::operator=(double z)

{
doublematrix *pm = m;
int i = n;
while(--i >= 0){
	// Matrix loeschen
	(*pm).setsize(0, 0);
	pm += 1;
	}
 if(fabs(z) > 0.0){
	doublematrix *ppm = m;
	int ii = r;
	while(--ii >= 0){
		(*ppm).setsize(rs, cs);
		*ppm = z;
		ppm += c + 1;
		}
	}
return *this;
}

doublesupermatrix &doublesupermatrix::operator=(doublematrix &z)

{
doublematrix *pm = m;
int i = n;
while(--i >= 0){
	// Matrix loeschen
	(*pm).setsize(0, 0);
	pm += 1;
	}
pm = m;
i = r;
while(--i >= 0){
	*pm = z;
	pm += c + 1;
	}
return *this;
}

doublesupermatrix &doublesupermatrix::operator=(const doublesupermatrix &z)

{
if(n != z.n){
	// Groesse kopieren
	n = z.n;
	// Gibt den Speicher frei (Test auf NULL nicht noetig?)
	delete[] m;
	// Speicher anfordern
	m = new doublematrix[n];
	}
// Abmessungen kopieren
r = z.r;
c = z.c;
// Abmessungen por Block kopieren
rs = z.rs;
cs = z.cs;
// Matrizen kopieren
int i = n;
doublematrix *pm = m;
doublematrix *pz = z.m;
while( --i >= 0){
	if(used(*pz)){
		*pm = *pz;
		}
	pz += 1;
	pm += 1;
	}
return *this;
}

//
// Die Super-Matrix
//

doubletempsupermatrix operator+(const doublesupermatrix &a)

{
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Und kopieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = *pa;
		}
	pa += 1;
	ps += 1;
	}
return s;
}

//
// Die Super-Matrix-Negation
//

doubletempsupermatrix operator-(const doublesupermatrix &a)

{
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Und kopieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = -(*pa);
		}
	pa += 1;
	ps += 1;
	}
return s;
}

//
// Die Super-Matrix-Multiplikation
//

doubletempsupermatrix operator*(const doublesupermatrix &a, const doublesupermatrix &b)

{
#ifdef DEBUG
if(a.c != b.r){
	cerr << "ERROR from doubletempsupermatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(a.r, b.c, a.rs, a.cs);
// Und multiplizieren
doublematrix *pa = a.m;
doublematrix *pb = b.m;
int j = a.c;
while(--j >= 0){
	doublematrix *ps = s.m;
	int i = s.r;
	while(--i >= 0){
		int k = s.c;
		while(--k >= 0){
			if(used(*pa) && used(*pb)){
				if(used(*ps)){
					*ps = *ps + *pa * *pb;
					}
				else{
					*ps = *pa * *pb;
					}
				}
			pb += 1;
			ps += 1;
			}
		pa += a.c;
		pb -= b.c;
		}
	pa -= a.n - 1;
	pb += b.c;
	}
return s;
}

//
// Die Super-Matrix-Addition
//

doubletempsupermatrix operator+(const doublesupermatrix &a, const doublesupermatrix &b)

{
#ifdef DEBUG
if(a.r != b.r || a.c != b.c){
	cerr << "ERROR from doubletempsupermatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Und addieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
doublematrix *pb = b.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa) && used(*pb)){
		*ps = *pa + *pb;
		}
	else if(used(*pa)){
		*ps = *pa;
		}
	else if(used(*pb)){
		*ps = *pb;
		}
	pa += 1;
	pb += 1;
	ps += 1;
	}
return s;
}

//
// Die Super-Matrix-Subtraktion
//

doubletempsupermatrix operator-(const doublesupermatrix &a, const doublesupermatrix &b)

{
#ifdef DEBUG
if(a.r != b.r || a.c != b.c){
	cerr << "ERROR from doubletempsupermatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Und addieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
doublematrix *pb = b.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa) && used(*pb)){
		*ps = *pa - *pb;
		}
	else if(used(*pa)){
		*ps = *pa;
		}
	else if(used(*pb)){
		*ps = -(*pb);
		}
	pa += 1;
	pb += 1;
	ps += 1;
	}
return s;
}

//
// Die Super-Matrix-Reell-Multiplikation
//

doubletempsupermatrix operator*(const doublesupermatrix &a, const doublematrix &b)

{
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Und multiplizieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = *pa * b;
		}
	pa += 1;
	ps += 1;
	}
return s;
}

doubletempsupermatrix operator*(const doublesupermatrix &a, const doublediagmatrix &b)

{
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Und multiplizieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = *pa * b;
		}
	pa += 1;
	ps += 1;
	}
return s;
}

doubletempsupermatrix operator*(const doublesupermatrix &a, double b)

{
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Und multiplizieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = *pa * b;
		}
	pa += 1;
	ps += 1;
	}
return s;
}

//
// Die Super-Matrix-Reell-Addition
//

doubletempsupermatrix operator+(const doublesupermatrix &a, const doublematrix &b)

{
#ifdef DEBUG
if(a.r != a.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = *pa;
		}
	pa += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps + b;
		}
	else{
		*ps = b;
		}
	ps += s.c + 1;
	}
return s;
}

doubletempsupermatrix operator+(const doublesupermatrix &a, const doublediagmatrix &b)

{
#ifdef DEBUG
if(a.r != a.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = *pa;
		}
	pa += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps + b;
		}
	else{
		*ps = b;
		}
	ps += s.c + 1;
	}
return s;
}

doubletempsupermatrix operator+(const doublesupermatrix &a, double b)

{
#ifdef DEBUG
if(a.r != a.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = *pa;
		}
	pa += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps + b;
		}
	else{
		(*ps).setsize(s.rs, s.cs);
		*ps = b;
		}
	ps += s.c + 1;
	}
return s;
}

//
// Die Super-Matrix-Reell-Subtraktion
//

doubletempsupermatrix operator-(const doublesupermatrix &a, const doublematrix &b)

{
#ifdef DEBUG
if(a.r != a.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = *pa;
		}
	pa += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps - b;
		}
	else{
		*ps = -b;
		}
	ps += s.c + 1;
	}
return s;
}

doubletempsupermatrix operator-(const doublesupermatrix &a, const doublediagmatrix &b)

{
#ifdef DEBUG
if(a.r != a.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = *pa;
		}
	pa += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps - b;
		}
	else{
		*ps = -b;
		}
	ps += s.c + 1;
	}
return s;
}

doubletempsupermatrix operator-(const doublesupermatrix &a, double b)

{
#ifdef DEBUG
if(a.r != a.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = *pa;
		}
	pa += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps - b;
		}
	else{
		(*ps).setsize(s.rs, s.cs);
		*ps = -b;
		}
	ps += s.c + 1;
	}
return s;
}

//
// Die Reell-Super-Matrix-Multiplikation
//

doubletempsupermatrix operator*(const doublematrix &a, const doublesupermatrix &b)

{
doubletempsupermatrix s(b.r, b.c, b.rs, b.cs);
// Und multiplizieren
doublematrix *ps = s.m;
doublematrix *pb = b.m;
int n = s.n;
while(--n >= 0){
	if(used(*pb)){
		*ps = a * *pb;
		}
	pb += 1;
	ps += 1;
	}
return s;
}

doubletempsupermatrix operator*(const doublediagmatrix &a, const doublesupermatrix &b)

{
doubletempsupermatrix s(b.r, b.c, b.rs, b.cs);
// Und multiplizieren
doublematrix *ps = s.m;
doublematrix *pb = b.m;
int n = s.n;
while(--n >= 0){
	if(used(*pb)){
		*ps = a * *pb;
		}
	pb += 1;
	ps += 1;
	}
return s;
}

doubletempsupermatrix operator*(double a, const doublesupermatrix &b)

{
doubletempsupermatrix s(b.r, b.c, b.rs, b.cs);
// Und multiplizieren
doublematrix *ps = s.m;
doublematrix *pb = b.m;
int n = s.n;
while(--n >= 0){
	if(used(*pb)){
		*ps = a * *pb;
		}
	pb += 1;
	ps += 1;
	}
return s;
}

//
// Die Reell-Super-Matrix-Addition
//

doubletempsupermatrix operator+(const doublematrix &a, const doublesupermatrix &b)

{
#ifdef DEBUG
if(b.r != b.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(b.r, b.c, b.rs, b.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pb = b.m;
int n = s.n;
while(--n >= 0){
	if(used(*pb)){
		*ps = *pb;
		}
	pb += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps + a;
		}
	else{
		*ps = a;
		}
	ps += s.c + 1;
	}
return s;
}

doubletempsupermatrix operator+(const doublediagmatrix &a, const doublesupermatrix &b)

{
#ifdef DEBUG
if(b.r != b.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(b.r, b.c, b.rs, b.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pb = b.m;
int n = s.n;
while(--n >= 0){
	if(used(*pb)){
		*ps = *pb;
		}
	pb += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps + a;
		}
	else{
		*ps = a;
		}
	ps += s.c + 1;
	}
return s;
}

doubletempsupermatrix operator+(double a, const doublesupermatrix &b)

{
#ifdef DEBUG
if(b.r != b.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(b.r, b.c, b.rs, b.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pb = b.m;
int n = s.n;
while(--n >= 0){
	if(used(*pb)){
		*ps = *pb;
		}
	pb += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps + a;
		}
	else{
		(*ps).setsize(s.rs, s.cs);
		*ps = a;
		}
	ps += s.c + 1;
	}
return s;
}

//
// Die Reell-Super-Matrix-Subtraktion
//

doubletempsupermatrix operator-(const doublematrix &a, const doublesupermatrix &b)

{
#ifdef DEBUG
if(b.r != b.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(b.r, b.c, b.rs, b.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pb = b.m;
int n = s.n;
while(--n >= 0){
	if(used(*pb)){
		*ps = -(*pb);
		}
	pb += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps + a;
		}
	else{
		*ps = a;
		}
	ps += s.c + 1;
	}
return s;
}

doubletempsupermatrix operator-(const doublediagmatrix &a, const doublesupermatrix &b)

{
#ifdef DEBUG
if(b.r != b.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(b.r, b.c, b.rs, b.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pb = b.m;
int n = s.n;
while(--n >= 0){
	if(used(*pb)){
		*ps = -(*pb);
		}
	pb += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps + a;
		}
	else{
		*ps = a;
		}
	ps += s.c + 1;
	}
return s;
}

doubletempsupermatrix operator-(double a, const doublesupermatrix &b)

{
#ifdef DEBUG
if(b.r != b.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(b.r, b.c, b.rs, b.cs);
// Erst kopieren
doublematrix *ps = s.m;
doublematrix *pb = b.m;
int n = s.n;
while(--n >= 0){
	if(used(*pb)){
		*ps = -(*pb);
		}
	pb += 1;
	ps += 1;
	}
// Und addieren
ps = s.m;
int i = s.r;
while(--i >= 0){
	if(used(*ps)){
		*ps = *ps + a;
		}
	else{
		(*ps).setsize(s.rs, s.cs);
		*ps = a;
		}
	ps += s.c + 1;
	}
return s;
}

//
// Die Super-Matrix-Inversion
//

doubletempsupermatrix inv(const doublesupermatrix &a)

{
int	i, j, k;

#ifdef DEBUG
if(a.r != a.c){
	cerr << "ERROR from doubletempsupermatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempsupermatrix s(a.r, a.c, a.rs, a.cs);
// Und kopieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int n = s.n;
while(--n >= 0){
	if(used(*pa)){
		*ps = *pa;
		}
	pa += 1;
	ps += 1;
	}
// Temporaerer Speicher
n = s.r;
doublematrix *ff = new doublematrix[n];
doublematrix *xx = new doublematrix[n];
int *ii = new int[n];
// Zeilennummern vorbesetzen
for(i = 0; i < n; i++){
	ii[i] = i;
	}
// Und invertieren
// Current line
doublematrix *pz = s.m;
for(i = 0; i < n; i++){
	double max = 0;
	// groessten Wert suchen
	if(used(pz[i])){
		max = norm(pz[i]);
		}
	int m = i;
	// Max Line
	doublematrix *pm = pz;
	// Next Line
	doublematrix *pn = pz;
	for(j = i + 1; j < n; j++){
		pn += n;
		if(used(pn[i])){
			double cmp = norm(pn[i]);
			if(cmp > max){
				max = cmp;
				m = j;
				pm = pn;
				}
			}
		}
	// Gegebenenfalls vertauschen
	if(m != i){
		for(k = 0; k < n; k++){
			if(used(pz[k])){
				xx[k] = pz[k];
				}
			else{
				xx[k].setsize(0,0);
				}
			if(used(pm[k])){
				pz[k] = pm[k];
				}
			else{
				pz[k].setsize(0,0);
				}
			if(used(xx[k])){
				pm[k] = xx[k];
				}
			else{
				pm[k].setsize(0,0);
				}
			}
		// Vertauschung merken
		int jj = ii[i];
		ii[i] = ii[m];
		ii[m] = jj;
		}
	ff[i] = inv(pz[i]);
	pz[i] = 1;
	// Durch alle Zeilen
	ps = s.m;
	j = n;
	while(--j >= 0){
		// Ausser der aktuellen Zeile
		if(ps != pz && used(ps[i])){
			// Faktor festlegen
			doublematrix cf = -ps[i] * ff[i];
			ps[i].setsize(0, 0);
			// Zeilen subtrahieren
			int kk = n;
			while(--kk >= 0){
				if(used(*ps)){
					*ps = *ps + cf * *pz;
					}
				else{
					*ps = cf * *pz;
					}
				ps += 1;
				pz += 1;
				}
			pz -= n;
			}
		else{
			ps += n;
			}
		}
	// Naechste Zeile;
	pz += n;
	}
// Und zuruecktauschen
ps = s.m;
for(i = 0; i < n; i++){
	for(j = 0; j < n; j++){
		if(used(ps[j])){
			xx[j] = ps[j];
			}
		else{
			xx[j].setsize(0, 0);
			}
		}
	for(j = 0; j < n; j++){
		if(used(xx[j])){
			ps[ii[j]] = ff[i] * xx[j];
			}
		else{
			ps[ii[j]].setsize(0, 0);
			}
		}
	ps += n;
	}
// Temporaeren Speicher wieder freigeben
delete[] ii;
delete[] xx;
delete[] ff;
return s;
}

//
// Die transponierte Super-Matrix
//

doubletempsupermatrix transp(const doublesupermatrix &a)

{
doubletempsupermatrix s(a.c, a.r, a.rs, a.cs);
// Und transponieren
doublematrix *ps = s.m;
doublematrix *pa = a.m;
int i = s.r;
while(--i >= 0){
	int j = s.c;
	while(--j >= 0){
		if(used(*pa)){
			*ps = transp(*pa);
			}
		ps += 1;
		pa += a.c;
		}
	pa -= a.n - 1;
	}
return s;
}
